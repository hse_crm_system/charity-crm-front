import 'package:flutter/material.dart';

const authApiUrl = "conflog.infostrategic.com";
const apiSubPath = '/crm-api';
// const authApiUrl = "sts.crmapix.xyz";

const kSpaceColor = Color(0x00FFE5F8);
const kPrimaryColor = Color(0xFF366CF6);
const kSecondaryColor = Color(0xFFF5F6FC);
const kBgLightColor = Color(0xFFF2F4FC);
const kBgDarkColor = Color(0xFFDFE1F0);
const kBadgeColor = Color(0xFFEE376E);
const kGrayColor = Color(0xFF8793B2);
const kTitleTextColor = Color(0xFF30384D);
const kTextColor = Color(0xFF4D5875);

const kDefaultPadding = 20.0;
