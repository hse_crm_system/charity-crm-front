import 'package:flutter/material.dart';
import 'package:flutter_crm_app/main.dart';

class RedirectNavigator {
  static String prevPath = '/';
  static String currentPath = '/';
  static void pushAndClean(String path, [Map<String, String> args]) {
    prevPath = currentPath;
    currentPath = path;
    navigatorKey.currentState.pushNamedAndRemoveUntil(
        path, (Route<dynamic> route) => false,
        arguments: args);

    // if (kIsWeb) {
    //   Future.delayed(
    //       Duration(milliseconds: 100),
    //       () => window.history.replaceState(null, null,
    //           "${window.location.href.split('/').take(window.location.href.split('/').length - 1).join('/')}$path"));
    // }
  }

  static void push(String path, [Map<String, String> args]) {
    prevPath = currentPath;
    currentPath = path;
    navigatorKey.currentState.pushNamedAndRemoveUntil(
        path, (Route<dynamic> route) => true,
        arguments: args);
  }

  static void back() {
    navigatorKey.currentState
        .pushNamedAndRemoveUntil(prevPath, (Route<dynamic> route) => false);
  }
}
