import 'package:flutter/material.dart';
import 'package:flutter_crm_app/components/rounded_input_field.dart';
import 'package:flutter_crm_app/main.dart';

void showCustomDialog(String title, String text, [Function onClose]) {
  showDialog(
      context: navigatorKey.currentContext,
      builder: (context) => Center(
              child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 450),
            child: AlertDialog(
              title: Text(title),
              content: SingleChildScrollView(child: Text(text)),
              actions: <Widget>[
                TextButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                    if (onClose != null) {
                      onClose();
                    }
                  },
                ),
              ],
            ),
          )));
}

void showPriceDialog(String title, String text, String label,
    [Function onClose, String submitText]) {
  final _textController = new TextEditingController();
  showDialog(
      context: navigatorKey.currentContext,
      builder: (context) => Center(
              child: AlertDialog(
            title: Text(title),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text(text),
                  SizedBox(
                    height: 10,
                  ),
                  RoundedInputField(
                    controller: _textController,
                    icon: Icons.monetization_on_outlined,
                    hintText: label,
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10.0, vertical: 4),
                  child: Text(submitText ?? 'Ok'),
                ),
                onPressed: () {
                  try {
                    if (onClose != null) {
                      onClose(_textController.value.text);
                    }
                    Navigator.of(context).pop();
                  } catch (e) {
                    showCustomDialog('Error', e.toString());
                  }
                },
              ),
            ],
          )));
}
