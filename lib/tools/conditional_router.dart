import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_crm_app/Screens/Loading/loading_screen.dart';
import 'package:flutter_crm_app/redux/data_state.dart';

class ConditionalRouter extends MapMixin<String, WidgetBuilder> {
  final Map<String, WidgetBuilder> public;
  final Map<String, WidgetBuilder> private;
  final LoadingState loadingState;

  ConditionalRouter(this.loadingState, {this.public, this.private});

  @override
  WidgetBuilder operator [](Object key) {
    if (public.containsKey(key)) return public[key];
    if (private.containsKey(key)) {
      if (loadingState == LoadingState.loaded) return private[key];
      return (context) => LoadingScreen();

      // return (context) => LoginScreen();
    }
    return null;
  }

  @override
  void operator []=(key, value) {}

  @override
  void clear() {}

  @override
  Iterable<String> get keys {
    final set = Set<String>();
    set.addAll(public.keys);
    set.addAll(private.keys);
    return set;
  }

  @override
  WidgetBuilder remove(Object key) {
    return public[key] ?? private[key];
  }
}
