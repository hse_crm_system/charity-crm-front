import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:universal_html/html.dart' as html;
import 'package:url_launcher/url_launcher.dart';

Future<String> downloadFile(String url) async {
  HttpClient httpClient = new HttpClient();
  File file;
  String filePath = '';

  try {
    var request = await httpClient.getUrl(Uri.parse(url));
    var response = await request.close();
    if (response.statusCode == 200) {
      var bytes = await consolidateHttpClientResponseBytes(response);
      filePath = '${getRandomString(15)}';
      file = File(filePath);
      await file.writeAsBytes(bytes);
    } else
      filePath = 'Error code: ' + response.statusCode.toString();
  } catch (ex) {
    filePath = 'Can not fetch url';
  }

  return filePath;
}

const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

void downloadFileWeb(String url) async {
  if (kIsWeb) {
    html.AnchorElement anchorElement = new html.AnchorElement(href: url);
    anchorElement.target = '_blank';
    anchorElement.download = url;
    anchorElement.click();
  } else if (Platform.isIOS || Platform.isAndroid) {
    if (await canLaunch(url)) await launch(url);
  }
}
