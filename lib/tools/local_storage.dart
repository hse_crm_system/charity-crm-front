import 'package:flutter_crm_app/models/login_user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  static final Future<SharedPreferences> _prefs =
      SharedPreferences.getInstance();

  static Future<Tokens> getTokens() async {
    final _preferences = await _prefs;
    final refresh = _preferences.getString("refresh");
    final access = _preferences.getString("access");
    if (refresh != null && access != null) {
      return new Tokens(access: access, refresh: refresh);
    }
    return null;
  }

  static void setTokens(Tokens tokens) async {
    final _preferences = await _prefs;
    _preferences.setString('access', tokens.access);
    _preferences.setString('refresh', tokens.refresh);
  }

  static Future<String> getValue(String name) async {
    final _preferences = await _prefs;
    return _preferences.getString(name);
  }

  static void setValue(String name, String value) async {
    final _preferences = await _prefs;
    _preferences.setString(name, value);
  }

  static void clearTokens() async {
    final _preferences = await _prefs;
    _preferences.remove('access');
    _preferences.remove('refresh');
  }
}
