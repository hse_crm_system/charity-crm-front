import 'dart:convert';

import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/main.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:http/http.dart' as http;

class CategoriesApi {
  static Future<Category> getCategoryById(String id) async {
    final qparams = {"id": id};
    final response = await http.get(
      Uri.https(authApiUrl, apiSubPath + '/Category/get_id', qparams),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}',
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      return Category(data['name'], data['description'], data['id'].toString());
    }
    return null;
  }

  static Future<List<Category>> getCategories(
      [int limit = 50, int offset = 0]) async {
    final qparams = {"offset": offset.toString(), "limit": limit.toString()};
    final response = await http.get(
      Uri.https(authApiUrl, apiSubPath + '/Category/get_all', qparams),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}',
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data["categoryList"] == null) {
        return null;
      }
      return (data['categoryList'] as List<dynamic>)
          .map((e) =>
              new Category(e['name'], e['description'], e['id'].toString()))
          .toList();
    }
    return null;
  }
}
