import 'dart:async';
import 'dart:convert';

import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/main.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:http/http.dart' as http;

class UserApi {
  static Future<List<Statistic>> getUserStatistic() async {
    // return Future.delayed(Duration(seconds: 2), () {
    //   return [
    //     Statistic("Всего жертвований", "22"),
    //     Statistic("Номер2", "22"),
    //     Statistic("Номер4", "Номер2"),
    //     Statistic("Номер223 жертвований", "22"),
    //     Statistic("Номер2 жертвований", "22"),
    //     Statistic("Что-то еще", "Значение")
    //   ];
    // });
    final response = await http.get(
      Uri.https(authApiUrl, apiSubPath + '/User/get_statistic'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}'
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      return ((data['statistic'] ?? []) as List<dynamic>)
          .map((e) => new Statistic(e['title'], e['value'].toString()))
          .toList();
    }
    return null;
  }

  static Future<Tokens> refreshTokens(Tokens oldTokens, String username) async {
    final tokensData = {
      "userName": username,
      "grantType": "refresh_token",
      "refreshToken": oldTokens.refresh
    };
    final response = await http.post(
        Uri.https(
          authApiUrl,
          apiSubPath + '/User/auth',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(tokensData));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data["accessToken"] == null || data["refreshToken"] == null) {
        return null;
      }
      return new Tokens(
          access: data["accessToken"], refresh: data["refreshToken"]);
    }
    return null;
  }

  static sendReqToBenificiar([String comment = '']) async {
    final data = {
      "comment": comment,
    };
    final response = await http.post(
        Uri.https(
          authApiUrl,
          apiSubPath + '/Beneficiary/create',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}'
        },
        body: jsonEncode(data));
    if (response.statusCode == 200) {
      return true;
    }
    return null;
  }

  static Future<Tokens> fetchTokens(LoginUser user) async {
    final userData = user.toMap();
    userData.addAll({
      // "client_id": "VotingApi",
      "grantType": "password"
    });
    final response = await http.post(
        Uri.https(
          authApiUrl,
          apiSubPath + '/User/auth',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(userData));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data["accessToken"] == null || data["refreshToken"] == null) {
        return null;
      }
      return new Tokens(
          access: data["accessToken"], refresh: data["refreshToken"]);
    }
    return null;
  }

  static Future<Map<String, dynamic>> regUser(RegUserData user) async {
    final userData = user.toMap();
    userData.addAll({"client_id": "VotingApi", "grant_type": "password"});
    final response = await http.post(
        Uri.https(
          authApiUrl,
          apiSubPath + '/User/registration',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(userData));
    if (response.statusCode == 200) {
      return {'status': 'ok'};
    }
    if (response.statusCode == 400) {
      return {
        'status': 'err',
        'message': (jsonDecode(response.body)['errors'] as Map<String, dynamic>)
            .values
            .first[0]
            .toString()
      };
    }
    return {'status': 'err', 'message': 'Произошла неизвестная ошибка'};
  }
}
