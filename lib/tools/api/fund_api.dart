import 'dart:convert';

import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/main.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:http/http.dart' as http;

class FundApi {
  static Future<List<FundModel>> getFunds(String searchTerm,
      [String categoryId, int limit = 10, int offset = 0]) async {
    final data = {
      "searchTerm": searchTerm,
      "onlyActive": true,
      "offset": offset,
      "limit": limit,
      "categoryId": categoryId
    };
    final response = await http.put(
        Uri.https(
          authApiUrl,
          apiSubPath + '/Foundation/search',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}'
        },
        body: jsonEncode(data));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data["foundations"] == null) {
        return null;
      }
      return (data['foundations'] as List<dynamic>)
          .map((e) => new FundModel(
              e['name'],
              e['description'],
              ((data['types'] ?? []) as List<dynamic>)
                  .map((e) => e.toString())
                  .toList(),
              e['id'].toString()))
          .toList();
    }
    return null;
  }

  static Future<FundModel> getFundById(String id) async {
    final response = await http.get(
      Uri.https(authApiUrl, apiSubPath + '/Foundation/get_id', {"request": id}),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}'
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }

      return new FundModel(
          data['name'],
          data['description'],
          ((data['categoryNames'] ?? []) as List<dynamic>)
              .map((e) => e.toString())
              .toList(),
          data['id'].toString(),
          ((data['statistic'] ?? []) as List<dynamic>)
              .map((e) => new Statistic(e['title'], e['value'].toString()))
              .toList());
    }
    return null;
  }
}
