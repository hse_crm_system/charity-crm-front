import 'dart:convert';

import 'package:file_picker/file_picker.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/main.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/tools/api/categories_api.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class ApplicationApi {
  static Future<bool> createApplication(ApplicationCreation data) async {
    final response = await http.post(
        Uri.https(
          authApiUrl,
          apiSubPath + '/Application/create',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}'
        },
        body: jsonEncode(data.toMap()));
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }

  static Future<bool> createTransaction(
      String applicationId, double value) async {
    final response = await http.post(
        Uri.https(
          authApiUrl,
          apiSubPath + '/Transaction/create',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}'
        },
        body: jsonEncode({"applicationId": applicationId, "value": value}));
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }

  static Future<FullCardData> getApplicationById(
    String id,
  ) async {
    final response = await http.get(
      Uri.https(
          authApiUrl, apiSubPath + '/Application/get_id', {"request": id}),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}'
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      final categoryId = data['categoryId'].toString();
      Category category;
      List<MediaFile> files = [];
      try {
        category = await CategoriesApi.getCategoryById(categoryId);
      } catch (e) {
        print('category err');
        print(e);
      }
      try {
        files =
            (((data['files'] ?? {'files': []})['files'] ?? []) as List<dynamic>)
                .map((e) => new MediaFile(e['uploadName'], e['path']))
                .toList();
      } catch (e) {
        print('files err');
        print(e);
      }

      return new FullCardData(
        data['title'],
        id.toString(),
        data['foundationName'] ?? "Нет фонда",
        DateTime.tryParse(data['expectedGatheringEnd']) ?? DateTime(2000),
        data['alreadyRaised']?.ceil() ?? 0,
        data['expectedByFund']?.ceil() ?? data['expectedSum']?.ceil() ?? 1,
        category?.name ?? "Неизвесттная категория",
        data['description'],
        files,
        data['foundationId']?.toString() ?? "-1",
        data['applicationStatus'],
      );
    }
    return null;
  }

  static Future<List<MediaFile>> uploadFiles(List<PlatformFile> files) async {
    var request = new http.MultipartRequest(
      "POST",
      Uri.https(authApiUrl, apiSubPath + '/FileLoader/load_files_temp'),
    );
    request.headers['Authorization'] =
        'Bearer ${store.state?.userState?.tokens?.access}';
    files.forEach((element) {
      request.files
          .add(http.MultipartFile.fromBytes('uploadedFiles', element.bytes,
              filename: element.name,
              // contentType: new MediaType('application', element.extension)
              contentType: new MediaType('application', 'octet-stream')));
    });
    final resp = await request.send();
    final response = await http.Response.fromStream(resp);

    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data['files'] == null) {
        return null;
      }
      return (data['files'] as List<dynamic>)
          .map((e) => new MediaFile(e['uploadName'], e['path']))
          .toList();
    }
    return null;
  }

  static Future<List<MyDonation>> getApplicationTransactions(
      String applicationId) async {
    final response = await http.put(
        Uri.https(authApiUrl,
            apiSubPath + '/Transaction/get_application_transactions'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}',
        },
        body: jsonEncode({"applicationId": applicationId}));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data["transactions"] == null) {
        return null;
      }
      return (data['transactions'] as List<dynamic>)
          .map((e) => new MyDonation(
              DateTime.tryParse(e['creationTime'] ?? "") ?? DateTime.now(),
              e['value'].toString()))
          .toList();
    }
    return null;
  }

  static Future<List<ApplicationReport>> getApplicationReports(
      String applicationId) async {
    final response = await http.get(
      Uri.https(authApiUrl, apiSubPath + '/Report/getApplicationReports',
          {"offset": "0", "limit": "2000", "applicationId": applicationId}),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}',
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data["reports"] == null) {
        return null;
      }
      return (data['reports'] as List<dynamic>)
          .map((e) => new ApplicationReport(
                e['title'],
                e['id'].toString(),
                e['applicationId'].toString(),
                e['description'],
                (((e['files'] ?? {"files": []})["files"] ?? [])
                        as List<dynamic>)
                    .map((t) => new MediaFile(
                        t['uploadName'].toString(), t['path'].toString()))
                    .toList(),
                DateTime.tryParse(e['creationTime'] ??
                        DateTime.now().toIso8601String()) ??
                    DateTime(2000),
              ))
          .toList();
    }
    return null;
  }

  static Future<List<CardData>> getMyTransactedApplications(
      int offset, int limit) async {
    final response = await http.put(
        Uri.https(
            authApiUrl, apiSubPath + '/Transaction/get_donated_applications'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}',
        },
        body: jsonEncode({"offset": offset, "limit": limit}));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data["donatedApplications"] == null) {
        return null;
      }
      return (data['donatedApplications'] as List<dynamic>)
          .map((e) => new CardData(
              e['title'],
              e['id'].toString(),
              e['foundationName'] ?? "Нет фонда",
              DateTime.tryParse(e['expectedGatheringEnd'] ??
                      DateTime.now().toIso8601String()) ??
                  DateTime(2000),
              e['alreadyRaised']?.ceil() ?? 0,
              e['expectedByFund']?.ceil() ?? e['expectedSum']?.ceil() ?? 1,
              e['categoryName']))
          .toList();
    }
    return null;
  }

  static Future<List<CardData>> getAllApplications(int offset, int limit,
      [String categoryId]) async {
    final response = await http.post(
        Uri.https(
            authApiUrl, apiSubPath + '/Transaction/get_donatable_applications'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}',
        },
        body: jsonEncode(
            {"offset": offset, "limit": limit, "categoryId": categoryId}));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data["applications"] == null) {
        return null;
      }
      return (data['applications'] as List<dynamic>)
          .map((e) => new CardData(
              e['title'],
              e['id'].toString(),
              e['foundationName'] ?? "Нет фонда",
              DateTime.tryParse(e['expectedGatheringEnd'] ??
                      DateTime.now().toIso8601String()) ??
                  DateTime(2000),
              e['alreadyRaised']?.ceil() ?? 0,
              // e['expectedSum']?.ceil() ?? 1,
              e['expectedByFund']?.ceil() ?? e['expectedSum']?.ceil() ?? 1,
              e['categoryName']))
          .toList();
    }
    return null;
  }

  static Future<List<CardData>> getMyApplications(int offset, int limit,
      [String categoryId]) async {
    final response = await http.post(
        Uri.https(authApiUrl, apiSubPath + '/Application/get_my_applications'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}',
        },
        body: jsonEncode(
            {"offset": offset, "limit": limit, "categoryId": categoryId}));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data["applications"] == null) {
        return null;
      }
      return (data['applications'] as List<dynamic>)
          .map((e) => new CardData(
              e['title'],
              e['id'].toString(),
              e['foundationName'] ?? "Нет фонда",
              DateTime.tryParse(e['expectedGatheringEnd'] ??
                      DateTime.now().toIso8601String()) ??
                  DateTime(2000),
              e['alreadyRaised']?.ceil() ?? 0,
              e['expectedByFund']?.ceil() ?? e['expectedSum']?.ceil() ?? 1,
              e['categoryName'],
              e['applicationStatus']))
          .toList();
    }
    return null;
  }

  static Future<List<Message>> getApplicationChat(String id) async {
    final response = await http.post(
        Uri.https(authApiUrl, apiSubPath + '/Chat/get_application_chat'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}',
        },
        body: jsonEncode({"applicationId": id, "offset": 0, "limit": 1200}));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      if (data == null) {
        return null;
      }
      if (data["messages"] == null) {
        return null;
      }
      return (data['messages'] as List<dynamic>).map((e) {
        List<MediaFile> files = [];
        try {
          files =
              (((e['media'] ?? {'media': []})['files'] ?? []) as List<dynamic>)
                  .map((x) => new MediaFile(x['uploadName'], x['path']))
                  .toList();
        } catch (e) {
          print('files err');
          print(e);
        }
        return new Message(
            DateTime.tryParse(e['creationTime'].toString()) ?? DateTime.now(),
            e['message'],
            store.state.userState.userData.username == e['username']
                ? "Вы"
                : "${e['username']} (${e['fullName']})",
            files);
      }).toList();
    }
    return null;
  }

  static Future<List<MediaFile>> sendMessage(
      String applicationId, Message message) async {
    final response = await http.post(
        Uri.https(
          authApiUrl,
          apiSubPath + '/Chat/send',
        ),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ${store.state?.userState?.tokens?.access}'
        },
        body: jsonEncode({
          "applicationId": applicationId,
          "message": message.text,
          "tempMedia": {"files": message.files.map((e) => e.toMap()).toList()}
        }));
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      List<MediaFile> files = [];
      try {
        files =
            (((data['files'] ?? {'files': []})['files'] ?? []) as List<dynamic>)
                .map((e) => new MediaFile(e['uploadName'], e['path']))
                .toList();
      } catch (e) {
        print('files err');
        print(e);
        return null;
      }
      return files;
    }
  }
}
