class CardData {
  String title;
  String id;
  String fund;
  String categoryName;
  DateTime endDate;
  int collectedSum;
  int targetSum;
  String status;
  CardData(this.title, this.id, this.fund, this.endDate, this.collectedSum,
      this.targetSum, this.categoryName,
      [this.status]);
}

class FullCardData extends CardData {
  String description;
  List<MediaFile> files;
  String fundId;

  FullCardData(title, id, fund, endDate, collectedSum, targetSum, type,
      this.description, this.files, this.fundId,
      [status])
      : super(title, id, fund, endDate, collectedSum, targetSum, type, status);
}

class ApplicationReport {
  String title;
  String id;
  String applicationId;
  String description;
  List<MediaFile> files;
  DateTime creationTime;
  ApplicationReport(this.title, this.id, this.applicationId, this.description,
      this.files, this.creationTime);
}

class ApplicationCreation extends CardData {
  String description;
  List<MediaFile> files;
  List<String> fundsId;

  ApplicationCreation(
      String title,
      String id,
      String fund,
      DateTime endDate,
      int collectedSum,
      int targetSum,
      String categoryId,
      this.description,
      this.files,
      this.fundsId)
      : super(title, id, fund, endDate, collectedSum, targetSum, categoryId);
  toMap() {
    return {
      "title": title,
      "description": description,
      "categoryId": categoryName,
      "foundationIds": fundsId,
      "expectedSum": targetSum,
      "expectedGatheringEnd": endDate.toIso8601String(),
      "temporaryUploadedFiles": {"files": files.map((e) => e.toMap()).toList()}
    };
  }
}

// enum ApplicationStatus { NotStarted, InProcess, Approved, Declined }
class MediaFile {
  String path;
  String uploadName;
  MediaFile(this.uploadName, this.path);
  toMap() {
    return {"uploadName": uploadName, "path": path};
  }
}

class MyDonation {
  DateTime date;
  String value;
  MyDonation(this.date, this.value);
}

class Message {
  DateTime date;
  String text;
  String from;
  List<MediaFile> files = [];
  Message(this.date, this.text, this.from, [this.files]);
}

class FundModel {
  String name;
  String id;
  String description;
  List<String> types;
  List<Statistic> statistic = [];
  FundModel(this.name, this.description, this.types, this.id, [this.statistic]);
}

class Statistic {
  String title;
  String value;
  Statistic(this.title, this.value);
}

class FundPreview {
  String name;
  String id;
  FundPreview(this.name, this.id);
}

class Category {
  String name;
  String description;
  String id;
  Category(this.name, this.description, this.id);
}

String getStringStatus(String status) {
  switch (status) {
    case "Created":
      return "Фонды рассматривают заявку";
    case "NeedsSpecification":
      return "Требует дополнения";
    case "WaitsForVoting":
      return "Отправлена на голосование";
    case "Fundraising":
      return "Идет сбор средств";
    case "Implemented":
      return "Выполнена";
    case "WaitsForImplementation":
      return "Ожидает исполнения";
    case "FoundationsRefused":
      return "Ни один фонд не выбрал Вашу заявку";
    case "DeclinedPermanently":
      return "Полностью отменена";
    case "Cancelled":
      return "Отменена";
    default:
      return "Неизвестный статус";
  }
}
