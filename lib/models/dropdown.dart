class ListItem {
  String value;
  String name;
  Function onClick = () {};
  ListItem(this.value, this.name, [this.onClick]);
}
