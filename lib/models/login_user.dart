import 'package:flutter/material.dart';

@immutable
class LoginUser {
  const LoginUser({@required this.username, @required this.password});
  final String username;
  final String password;
  @override
  String toString() {
    return "$username - $password";
  }

  Map<String, dynamic> toMap() {
    return {
      'userName': username,
      'password': password,
    };
  }
}

@immutable
class RegUserData {
  const RegUserData(
      {@required this.email,
      @required this.firstName,
      @required this.lastName,
      @required this.password,
      @required this.isOrganisation,
      this.username});
  final String email;
  final String username;
  final String password;
  final String firstName;
  final String lastName;
  final bool isOrganisation;
  @override
  String toString() {
    return "$email - $password";
  }

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'userName': username,
      'password': password,
      'lastName': lastName,
      'firstName': firstName,
      'isOrganisation': isOrganisation
    };
  }
}

@immutable
class UserData {
  const UserData({
    @required this.username,
    @required this.sub,
    @required this.firstName,
    @required this.lastName,
    @required this.role,
  });
  final String username;
  final String firstName;
  final String lastName;
  final List<dynamic> role;
  final String sub;
  @override
  String toString() {
    return "$username";
  }

  Map<String, dynamic> toMap() {
    return {'username': username, 'sub': sub};
  }
}

@immutable
class Tokens {
  const Tokens({@required this.access, @required this.refresh});
  final String access;
  final String refresh;
  Map<String, dynamic> toMap() {
    return {
      'access_token': access,
      'refresh_token': refresh,
    };
  }

  @override
  String toString() {
    return {
      'access_token': access,
      'refresh_token': refresh,
    }.toString();
  }
}
