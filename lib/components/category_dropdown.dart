import 'package:flutter/material.dart';
import 'package:flutter_crm_app/components/dropdown_sort.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/models/dropdown.dart';
import 'package:flutter_crm_app/tools/api/categories_api.dart';

class CategoryDropdown extends StatefulWidget {
  final Function handler;
  final bool isNolimWidth;
  CategoryDropdown(this.handler, [this.isNolimWidth = false]);
  @override
  _CategoryDropdownState createState() => _CategoryDropdownState();
}

class _CategoryDropdownState extends State<CategoryDropdown> {
  List<Category> _allCategories = [];
  void _getCategories() async {
    try {
      final categories = await CategoriesApi.getCategories();
      if (categories != null) {
        setState(() {
          _allCategories = [
            new Category("Все категории", "Empty", null),
            ...categories
          ];
        });
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    _getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.isNolimWidth ? null : 120,
      child: (_allCategories.isNotEmpty)
          ? DropdownSort(
              _allCategories
                  .map((e) => ListItem(e.id, e.name, () {
                        setState(() {
                          widget.handler(e);
                        });
                      }))
                  .toList(),
              "Категория")
          : null,
    );
  }
}
