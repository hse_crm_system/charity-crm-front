import 'package:flutter/material.dart';
import 'package:flutter_crm_app/constants.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Function press;
  final bool isDisabled;
  final Color color, textColor, disabledColor;
  const RoundedButton({
    Key key,
    @required this.text,
    @required this.press,
    this.isDisabled = false,
    this.color = kPrimaryColor,
    this.disabledColor = kBgDarkColor,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.8,
      child: TextButton(
        onPressed: isDisabled
            ? null
            : () {
                press();
              },
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: isDisabled ? disabledColor : color),
          child: isDisabled
              ? Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 40),
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              : Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
                  child: Text(
                    text,
                    style: TextStyle(color: textColor),
                    textAlign: TextAlign.center,
                  ),
                ),
        ),
      ),
    );
  }
}
