import 'package:flutter/material.dart';
import 'package:flutter_crm_app/constants.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  final TextEditingController controller;
  final Function onSubmit;
  final bool isMultiline;
  const RoundedInputField(
      {Key key,
      this.hintText,
      this.icon = Icons.person,
      this.onChanged,
      this.onSubmit,
      this.controller,
      this.isMultiline = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kBgLightColor, borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 10),
        child: TextField(
          controller: controller,
          onChanged: onChanged,
          cursorColor: kPrimaryColor,
          onSubmitted: onSubmit,
          keyboardType: isMultiline ? TextInputType.multiline : null,
          maxLines: isMultiline ? null : 1,
          decoration: InputDecoration(
            icon: Icon(
              icon,
              color: kGrayColor,
            ),
            hintText: hintText,
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }
}
