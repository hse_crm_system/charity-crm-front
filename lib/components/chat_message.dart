import 'package:flutter/material.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:intl/intl.dart';

import 'file_carousel.dart';

class ChatMessage extends StatelessWidget {
  final Message message;
  const ChatMessage(this.message);
  @override
  Widget build(BuildContext context) {
    final local = Localizations.localeOf(context).languageCode;
    final isMine = message.from == "Вы";
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Text(
              DateFormat("dd.MM.yy", local).format(message.date.toLocal()) +
                  ' - ' +
                  message.from,
              style: TextStyle(color: kTextColor),
            ),
          ),
          SizedBox(
            height: 3,
          ),
          ConstrainedBox(
            constraints: BoxConstraints(minWidth: 120, minHeight: 40),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: isMine ? kGrayColor : Colors.white,
              ),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      message.text,
                      style: Theme.of(context).textTheme.button.copyWith(
                          color: isMine ? kBgLightColor : kTitleTextColor,
                          fontSize: 16),
                    ),
                    if (message.files != null && message.files?.length != 0)
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: FilesCarousel(
                            files: message.files ?? [],
                            isMineMessage: isMine,
                          ),
                        ),
                      )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
