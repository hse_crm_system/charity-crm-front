import 'package:flutter/material.dart';
import 'package:flutter_crm_app/constants.dart';

class RoundedPasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  final Function onSubmit;
  final String label;
  final TextEditingController controller;
  const RoundedPasswordField(
      {Key key,
      this.onSubmit,
      this.onChanged,
      this.controller,
      this.label = "Пароль"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: kBgLightColor, borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 10),
        child: TextField(
          controller: controller,
          obscureText: true,
          onSubmitted: onSubmit,
          onChanged: onChanged,
          cursorColor: kPrimaryColor,
          decoration: InputDecoration(
            hintText: label,
            icon: Icon(
              Icons.lock,
              color: kGrayColor,
            ),
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }
}
