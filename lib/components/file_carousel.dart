import 'package:flutter/material.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/tools/download_file.dart';

class FilesCarousel extends StatelessWidget {
  const FilesCarousel(
      {Key key,
      @required this.files,
      this.isRemoveable = false,
      this.onRemove,
      this.isMineMessage = false})
      : super(key: key);

  final List<MediaFile> files;
  final bool isRemoveable;
  final bool isMineMessage;
  final Function(MediaFile) onRemove;

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      thickness: 3,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Row(
            children: [
              ...files.map((e) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Column(
                      children: [
                        IconButton(
                            color: isMineMessage ? kBgLightColor : kTextColor,
                            icon: Icon(Icons.file_copy),
                            onPressed: () {
                              downloadFileWeb('https://' +
                                  authApiUrl +
                                  apiSubPath +
                                  e.path);
                            }),
                        Text(
                          (() {
                            if (e.uploadName == null) {
                              return getRandomString(3);
                            }
                            return files.lastIndexOf(e).toString() +
                                '.' +
                                e.uploadName.split('.').last;
                          })(),
                          style: TextStyle(
                              color:
                                  isMineMessage ? kBgLightColor : kTextColor),
                        ),
                        if (isRemoveable)
                          IconButton(
                              icon: Icon(
                                Icons.remove_circle_outline_outlined,
                                color: kBadgeColor,
                              ),
                              onPressed: () {
                                if (onRemove != null) {
                                  onRemove(e);
                                }
                              })
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
