import 'package:flutter/material.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/dropdown.dart';

class DropdownSort extends StatefulWidget {
  final List<ListItem> items;
  final String hintText;
  const DropdownSort(this.items, [this.hintText]);
  @override
  _DropdownSortState createState() => _DropdownSortState();
}

class _DropdownSortState extends State<DropdownSort> {
  List<DropdownMenuItem<ListItem>> _dropdownMenuItems;
  ListItem _selectedItem;

  void initState() {
    super.initState();
    _dropdownMenuItems = buildDropDownMenuItems(widget.items);
  }

  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<ListItem>> items = [];
    for (ListItem listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      child: DropdownButton<ListItem>(
          isExpanded: true,
          itemHeight: 70.0,
          hint: Text(widget.hintText ?? 'Выберите категорию'),
          elevation: 0,
          style: TextStyle(color: kGrayColor),
          value: _selectedItem,
          items: _dropdownMenuItems,
          onChanged: (value) {
            if (value.onClick != null) value.onClick();
            setState(() {
              _selectedItem = value;
            });
          }),
    );
  }
}
