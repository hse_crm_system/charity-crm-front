import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_crm_app/components/rounded_button.dart';
import 'package:flutter_crm_app/components/rounded_input_field.dart';
import 'package:flutter_crm_app/tools/api/user_api.dart';
import 'package:flutter_crm_app/tools/cross_platform_svg.dart';
import 'package:flutter_crm_app/tools/local_storage.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';

class SendBeneficiaryRequest extends StatefulWidget {
  @override
  _SendBeneficiaryRequestState createState() => _SendBeneficiaryRequestState();
}

class _SendBeneficiaryRequestState extends State<SendBeneficiaryRequest> {
  bool isLoading;
  bool isAlreadySend = false;
  final _commentController = new TextEditingController();
  void _sendRequestToSetBenificiar() async {
    try {
      bool resp =
          await UserApi.sendReqToBenificiar(_commentController.value.text);
      if (resp == null) {
        showCustomDialog('Ошибка', "Вы уже отправляли заявку", () {
          LocalStorage.setValue('isAlreadySend', "1");
        });
        setState(() {
          isAlreadySend = true;
        });
        return;
      }
      setState(() {
        isAlreadySend = true;
      });
      Timer(Duration(microseconds: 1000), () {
        showCustomDialog('Заявка отправлена на модерацию',
            "Среднее время рассмотрения заявки - 4 часа");
      });
    } catch (e) {
      print(e);
      showCustomDialog('Ошибка', "Произошла ошибка");
    }
  }

  @override
  void initState() {
    super.initState();
    (() async {
      try {
        setState(() {
          isLoading = true;
        });
        final lsValue = await LocalStorage.getValue('isAlreadySend');
        setState(() {
          isAlreadySend = (lsValue == "1");
        });
      } catch (e) {
        print(e);
      } finally {
        setState(() {
          isLoading = false;
        });
      }
    })();
  }

  @override
  void dispose() {
    _commentController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else if (isAlreadySend) {
      return Container(
        child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            Text(
              "Ваша заявка отправлена на проверку.",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Среднее время рассмотрения заявки - 4 часа',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14),
            ),
            SizedBox(
              height: 40,
            ),
            RoundedButton(
                isDisabled: true,
                text: 'Подать заявку на проверку данных',
                press: _sendRequestToSetBenificiar),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      );
    } else
      return Container(
        child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            Text(
              "Для создания заявки Ваши данные должны быть проверены модератором.",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(
              height: 40,
            ),
            RoundedInputField(
                controller: _commentController,
                icon: Icons.comment_rounded,
                hintText: "Комментарий к заявке"),
            CrossPlatformImage.asset(
              "assets/images/login.png",
              width: min(450, size.width - 20),
            ),
            SizedBox(
              height: 10,
            ),
            RoundedButton(
                text: 'Подать заявку на проверку данных',
                press: _sendRequestToSetBenificiar),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      );
  }
}
