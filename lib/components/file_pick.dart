import 'dart:math';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/tools/api/application_api.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';

import '../constants.dart';

class FilePick extends StatefulWidget {
  final Function(List<MediaFile>) handler;
  FilePick(this.handler);
  @override
  _FilePickState createState() => _FilePickState();
}

class _FilePickState extends State<FilePick> {
  bool _isLoading = false;
  void _addFile() async {
    FilePickerResult result =
        await FilePicker.platform.pickFiles(allowMultiple: true);

    if (result != null) {
      try {
        if (result.count > 15) {
          showCustomDialog("Ошибка",
              "Вы выбрали слишком много файлов. Максимальное количество файлов - 15");
          return;
        }
        result.files.forEach((element) {
          if (element.size > 20 * pow(10, 6)) {
            showCustomDialog("Ошибка",
                "Вы выбрали слишком большой файл. Максимальный размер - 20 МБ");
            return;
          }
        });

        setState(() {
          _isLoading = true;
        });
        final resp = await ApplicationApi.uploadFiles(result.files);
        if (resp != null) {
          widget.handler(resp);
        }
        if (resp == null) {
          showCustomDialog("Ошибка", "Ошибка загрузки файлов");
        }
      } catch (e) {
        showCustomDialog("Ошибка", "Произошла неизвестная ошибка");
        print(e);
      } finally {
        setState(() {
          _isLoading = false;
        });
      }
    } else {}
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _isLoading
          ? Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 14.0, horizontal: 20),
                child: Center(child: CircularProgressIndicator()),
              ),
            )
          : TextButton(
              onPressed: _addFile,
              child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 14.0, horizontal: 20),
                    child: Center(
                        child: Text(
                      "Добавить файл",
                      style: TextStyle(color: kGrayColor),
                    )),
                  ))),
    );
  }
}
