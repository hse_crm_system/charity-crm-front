import 'package:flutter/material.dart';
import 'package:flutter_crm_app/components/file_carousel.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/tools/api/application_api.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';

class ApplicationReports extends StatefulWidget {
  final String id;
  const ApplicationReports(this.id);

  @override
  _ApplicationReportsState createState() => _ApplicationReportsState();
}

class _ApplicationReportsState extends State<ApplicationReports> {
  List<ApplicationReport> _applicationReports = [];

  @override
  void initState() {
    super.initState();
    load();
  }

  void load() async {
    final appReports = await ApplicationApi.getApplicationReports(widget.id);
    if (appReports != null) {
      setState(() {
        _applicationReports = appReports;
      });
    } else {
      showCustomDialog('Ошибка', 'Ошибка загрузки отчётов');
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_applicationReports.length != 0)
      return Padding(
          padding: const EdgeInsets.symmetric(vertical: 18.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "ОТЧЁТЫ",
                style: Theme.of(context)
                    .textTheme
                    .button
                    .copyWith(color: kTextColor, fontSize: 14),
              ),
              ..._applicationReports.map((e) => Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(width: 1.0, color: Colors.black26),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            e.title,
                            style: Theme.of(context)
                                .textTheme
                                .button
                                .copyWith(color: kTextColor, fontSize: 16),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            e.description,
                            style: Theme.of(context)
                                .textTheme
                                .button
                                .copyWith(color: kTextColor, fontSize: 14),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          FilesCarousel(files: e.files)
                        ],
                      ),
                    ),
                  ))
            ],
          ));

    return SizedBox();
  }
}
