import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_crm_app/Screens/Create/create.dart';
import 'package:flutter_crm_app/Screens/Login/actions.dart';
import 'package:flutter_crm_app/redux/app_state.dart';
import 'package:flutter_crm_app/responsive.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';

import '../constants.dart';
import '../extensions.dart';
import 'side_menu_item.dart';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_redux_hooks/flutter_redux_hooks.dart' as hooks;
import 'package:flutter_hooks/flutter_hooks.dart' show HookWidget;

class SideMenu extends HookWidget {
  const SideMenu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dispatch = hooks.useDispatch<AppState>();
    void _onLogout() {
      dispatch(LogoutAction());
      Timer(Duration(microseconds: 300), () {
        RedirectNavigator.pushAndClean('/');
      });
    }

    final path = ModalRoute.of(context).settings.name;

    return Container(
      height: double.infinity,
      padding: EdgeInsets.only(top: kIsWeb ? kDefaultPadding : 0),
      color: kBgLightColor,
      child: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: kDefaultPadding),
          child: Column(
            children: [
              Row(
                children: [
                  if (Responsive.isMobile(context)) CloseButton(),
                ],
              ),
              SizedBox(height: kDefaultPadding),
              // ignore: deprecated_member_use
              FlatButton.icon(
                minWidth: double.infinity,
                padding: EdgeInsets.symmetric(
                  vertical: kDefaultPadding,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                color: kPrimaryColor,
                onPressed: () {
                  SchedulerBinding.instance.addPostFrameCallback((_) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Create()));
                  });
                },
                icon: Icon(
                  Icons.create_rounded,
                  size: 16,
                  color: Colors.white,
                ),
                label: Text(
                  "Создать заявку",
                  style: TextStyle(color: Colors.white),
                ),
              ).addNeumorphism(
                topShadowColor: Colors.white,
                bottomShadowColor: Color(0xFF234395).withOpacity(0.2),
              ),

              SizedBox(height: kDefaultPadding * 2),
              // Menu Items
              SideMenuItem(
                  press: () {
                    RedirectNavigator.pushAndClean('/marketplace');
                  },
                  title: "Домой",
                  icon: Icons.home_rounded,
                  isActive: path?.contains('/marketplace') ?? false),
              SideMenuItem(
                press: () {
                  RedirectNavigator.pushAndClean('/myReq');
                },
                title: "Мои заявки",
                icon: Icons.my_library_books,
                isActive: path?.contains('/myReq') ?? false,
              ),
              SideMenuItem(
                press: () {
                  RedirectNavigator.pushAndClean('/more');
                },
                title: "Еще",
                icon: Icons.more_horiz,
                isActive: path?.contains('/more') ?? false,
              ),
              SideMenuItem(
                press: _onLogout,
                title: "Выйти",
                icon: Icons.logout,
                isActive: false,
                showBorder: false,
              ),

              // SizedBox(height: kDefaultPadding * 2),
              // Tags(),
            ],
          ),
        ),
      ),
    );
  }
}
