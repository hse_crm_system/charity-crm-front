import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Login/actions.dart';
import 'package:flutter_crm_app/Screens/Login/reducer.dart';
import 'package:flutter_crm_app/redux/app_state.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux/redux.dart';

import 'epics.dart';

AppState reducer(AppState state, dynamic action) {
  return AppState(
    userState: userReducer(state.userState, action),
  );
}

@immutable
class AppStore {
  const AppStore({
    @required this.store,
  });

  factory AppStore.inject() {
    return AppStore(
      store: Store<AppState>(
        reducer,
        initialState: _injectedState(),
        middleware: [
          EpicMiddleware<AppState>(epics()),
        ],
      ),
    );
  }

  static AppState _injectedState() {
    return AppState.initialState();
  }

  final Store<AppState> store;

  Future dispatchInitial() => store.dispatch(LoadLocalTokensAction());
}
