import 'package:flutter_crm_app/Screens/Login/epic.dart';
import 'package:redux_epics/redux_epics.dart';
import 'app_state.dart';

Epic<AppState> epics() => combineEpics<AppState>([
      userEpic(),
    ]);
