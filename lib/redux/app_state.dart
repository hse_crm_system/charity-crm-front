import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Login/state.dart';

@immutable
class AppState {
  const AppState({
    @required this.userState,
  });

  factory AppState.initialState() {
    return AppState(
      userState: UserState.initialState(),
    );
  }

  AppState copyWith({
    @required UserState userState,
  }) {
    return AppState(
      userState: userState,
    );
  }

  final UserState userState;
}
