import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Error/error_screen.dart';
import 'package:flutter_crm_app/Screens/MyReq/my_req.dart';
import 'package:flutter_crm_app/Screens/Fund/fund.dart';
import 'package:flutter_crm_app/Screens/Login/login_screen.dart';
import 'package:flutter_crm_app/Screens/Marketplace/marketplace.dart';
import 'package:flutter_crm_app/Screens/More/more.dart';
import 'package:flutter_crm_app/redux/app_state.dart';
import 'package:flutter_crm_app/redux/core.dart';
import 'package:flutter_crm_app/redux/data_state.dart';
import 'package:flutter_crm_app/tools/conditional_router.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'Screens/Signup/signup_screen.dart';
import 'Screens/Welcome/welcome_screen.dart';
import 'constants.dart';
import 'package:flutter_redux_hooks/flutter_redux_hooks.dart' as hooks;
import 'package:flutter_hooks/flutter_hooks.dart' show HookWidget, useEffect;

final store = (AppStore.inject()..dispatchInitial()).store;
final navigatorKey = GlobalKey<NavigatorState>();
void main() {
  runApp(
    hooks.StoreProvider<AppState>(
      store: store,
      child: MyApp(),
    ),
  );
}

class MyApp extends HookWidget {
  @override
  Widget build(BuildContext context) {
    useEffect(() {
      initializeDateFormatting();
      return;
    }, []);
    final loadingState = hooks.useSelector<AppState, LoadingState>(
        (state) => state.userState.dataState.loadingState);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'GoodFund',
      navigatorKey: navigatorKey,
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: kBgLightColor,
      ),
      initialRoute: '/',
      routes: ConditionalRouter(loadingState, public: {
        '/': (context) => WelcomeScreen(),
        '/registration': (context) => SignUpScreen(),
        '/login': (context) => LoginScreen(),
        '/errorScreen': (context) => Scaffold(
              body: ErrorScreen(),
            ),
      }, private: {
        '/marketplace': (context) => Marketplace(),
        '/more': (context) => More(),
        '/myReq': (context) => MyRequests(),
        '/fund': (context) => Fund()
      }),
    );
  }
}
