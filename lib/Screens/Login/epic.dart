import 'package:flutter_crm_app/Screens/Login/actions.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/redux/app_state.dart';
import 'package:flutter_crm_app/tools/api/user_api.dart';
import 'package:flutter_crm_app/tools/local_storage.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_crm_app/redux/data_state.dart';

Epic<AppState> _fetchTokensEpic() =>
    (Stream<dynamic> actions, EpicStore<AppState> store) {
      return actions
          .whereType<FetchTokensAction>()
          .switchMap((action) => _fetchTokens(action.userData));
    };

Stream<dynamic> _fetchTokens(LoginUser data) async* {
  try {
    yield SetUserDataStateAction(DataState.loading());
    final resp = await UserApi.fetchTokens(data);
    if (resp != null) {
      yield SetTokensAction(resp);
    } else {
      throw new Exception("No response - _fetchTokens");
    }
  } on Exception catch (e) {
    print(e);
    yield SetLoginException(e);
    yield SetUserDataStateAction(DataState.error(e));
  }
}

Epic<AppState> _setTokensEpic() =>
    (Stream<dynamic> actions, EpicStore<AppState> store) {
      return actions
          .whereType<SetTokensAction>()
          .switchMap((action) => _setTokens(action.tokens));
    };

Stream<dynamic> _setTokens(Tokens tokens) async* {
  try {
    LocalStorage.setTokens(tokens);
    final data = Jwt.parseJwt(tokens.access);
    yield SetUserDataAction(new UserData(
        username: data["name"],
        sub: data['sub'],
        firstName: data['FirstName'],
        lastName: data['LastName'],
        role: (data['role'].runtimeType == String)
            ? [data['role']]
            : data['role']));
  } on Exception catch (e) {
    print(e);
    yield SetLoginException(e);
    yield SetUserDataStateAction(DataState.error(e));
  }
}

Epic<AppState> _loadTokensEpic() =>
    (Stream<dynamic> actions, EpicStore<AppState> store) {
      return actions
          .whereType<LoadLocalTokensAction>()
          .switchMap((action) => _loadTokens());
    };
Stream<dynamic> _loadTokens() async* {
  try {
    yield SetUserDataStateAction(DataState.loading());
    final Tokens tokens = await LocalStorage.getTokens();
    if (tokens != null) {
      yield RefreshTokensAction(tokens);
    } else {
      yield SetUserDataStateAction(DataState.never());
    }
  } on Exception catch (e) {
    print(e);
    yield SetLoginException(e);
    yield SetUserDataStateAction(DataState.error(e));
  }
}

Epic<AppState> _refreshTokensEpic() =>
    (Stream<dynamic> actions, EpicStore<AppState> store) {
      return actions
          .whereType<RefreshTokensAction>()
          .switchMap((action) => _refreshTokens(action.tokens));
    };
Stream<dynamic> _refreshTokens(Tokens tokens) async* {
  try {
    final Tokens newTokens = await UserApi.refreshTokens(
        tokens, Jwt.parseJwt(tokens.access)["name"]);
    if (newTokens != null) {
      yield SetTokensAction(newTokens);
    } else {
      LocalStorage.clearTokens();
      showCustomDialog('Error', 'Token update exception.', () {
        RedirectNavigator.pushAndClean('/');
      });
      yield SetUserDataStateAction(DataState.never());
    }
  } on Exception catch (e) {
    print(e);
    yield SetUserDataStateAction(DataState.never());
  }
}

Epic<AppState> _logoutEpic() =>
    (Stream<dynamic> actions, EpicStore<AppState> store) {
      return actions.whereType<LogoutAction>().switchMap((action) => _logout());
    };
Stream<dynamic> _logout() async* {
  try {
    yield SetUserDataAction(null);
    LocalStorage.clearTokens();
    LocalStorage.setValue('isAlreadySend', "0");
  } on Exception catch (e) {
    print(e);
    yield SetLoginException(e);
  } finally {
    yield SetUserDataStateAction(DataState.never());
  }
}

Epic<AppState> userEpic() => combineEpics<AppState>([
      _fetchTokensEpic(),
      _loadTokensEpic(),
      _refreshTokensEpic(),
      _setTokensEpic(),
      _logoutEpic()
    ]);
