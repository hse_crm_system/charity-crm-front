import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/redux/data_state.dart';

class FetchTokensAction {
  const FetchTokensAction(this.userData);
  final LoginUser userData;
}

class SetTokensAction {
  const SetTokensAction(this.tokens);
  final Tokens tokens;
}

class SetUserDataAction {
  const SetUserDataAction(this.userData);
  final UserData userData;
}

class SetUserDataStateAction {
  const SetUserDataStateAction(this.dataState);
  final DataState dataState;
}

class RefreshTokensAction {
  const RefreshTokensAction(this.tokens);
  final Tokens tokens;
}

class LoadLocalTokensAction {
  const LoadLocalTokensAction();
}

class LogoutAction {
  const LogoutAction();
}

class SetLoginException {
  const SetLoginException(this.exception);
  final Exception exception;
}
