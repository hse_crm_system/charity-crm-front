import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Login/components/background.dart';
import 'package:flutter_crm_app/components/already_have_an_account_acheck.dart';
import 'package:flutter_crm_app/components/rounded_button.dart';
import 'package:flutter_crm_app/components/rounded_input_field.dart';
import 'package:flutter_crm_app/components/rounded_password_field.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/redux/data_state.dart';
import 'package:flutter_crm_app/tools/cross_platform_svg.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';

class Body extends StatefulWidget {
  final Function onSubmit;
  final LoadingState loadingState;
  const Body(this.onSubmit, this.loadingState) : assert(onSubmit != null);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final _onSubmit = () {
      if (_usernameController.value.text.length == 0 ||
          _passwordController.value.text.length == 0) {
        showCustomDialog('Ошибка', 'Вы заполнили не все поля');
        return;
      }
      widget.onSubmit(new LoginUser(
          username: _usernameController.value.text,
          password: _passwordController.value.text));
    };
    return Background(
      child: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 450),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Вход",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                      color: kTextColor),
                ),
                SizedBox(height: size.height * 0.03),
                CrossPlatformImage.asset(
                  "assets/images/login.png",
                  height: size.height * 0.35,
                ),
                SizedBox(height: size.height * 0.03),
                RoundedInputField(
                  hintText: "Username",
                  controller: _usernameController,
                  onSubmit: (String _) => _onSubmit(),
                ),
                SizedBox(height: 10),
                RoundedPasswordField(
                  controller: _passwordController,
                  onSubmit: (String _) => _onSubmit(),
                ),
                RoundedButton(
                  text: "Войти",
                  press: _onSubmit,
                  isDisabled: widget.loadingState == LoadingState.loading,
                ),
                SizedBox(height: size.height * 0.03),
                AlreadyHaveAnAccountCheck(
                  press: () {
                    RedirectNavigator.pushAndClean('/registration');
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
