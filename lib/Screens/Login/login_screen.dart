import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_crm_app/Screens/Login/components/body.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/redux/app_state.dart';
import 'package:flutter_crm_app/redux/data_state.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';
import 'package:flutter_redux_hooks/flutter_redux_hooks.dart' as hooks;
import 'package:flutter_hooks/flutter_hooks.dart' show HookWidget, useEffect;

import 'actions.dart';

class LoginScreen extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final loadingState = hooks.useSelector<AppState, LoadingState>(
        (state) => state.userState.dataState?.loadingState);
    final dispatch = hooks.useDispatch<AppState>();
    useEffect(() {
      if (loadingState == LoadingState.loaded) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          RedirectNavigator.pushAndClean('/marketplace');
        });
      }
      if (loadingState == LoadingState.error) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          showCustomDialog(
              'Неверные данные', 'Вы ввели некорректные данные для входа');
        });
      }
      return;
    }, [loadingState]);
    void onSubmit(LoginUser formData) {
      dispatch(FetchTokensAction(formData));
    }

    return SafeArea(
      child: Scaffold(
        body: Body(onSubmit, loadingState),
      ),
    );
  }
}
