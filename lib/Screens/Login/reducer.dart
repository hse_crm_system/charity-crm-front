import 'package:flutter_crm_app/Screens/Login/state.dart';
import 'package:flutter_crm_app/redux/data_state.dart';

import 'actions.dart';

UserState userReducer(UserState state, dynamic action) {
  if (action is SetTokensAction) {
    return state.copyWith(
      tokens: action.tokens,
    );
  }
  if (action is SetUserDataAction) {
    return state.copyWith(
        userData: action.userData, dataState: DataState.loaded());
  }
  if (action is SetUserDataStateAction) {
    return state.copyWith(
      dataState: action.dataState,
    );
  }
  return state;
}
