import 'package:flutter/material.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/redux/data_state.dart';

@immutable
class UserState {
  const UserState({
    @required this.dataState,
    @required this.tokens,
    @required this.userData,
  });

  UserState.initialState()
      : dataState = DataState.never(),
        tokens = null,
        userData = null;

  final DataState dataState;
  final Tokens tokens;
  final UserData userData;

  UserState copyWith({
    DataState dataState,
    Tokens tokens,
    UserData userData,
  }) {
    return UserState(
        dataState: dataState ?? DataState.never(),
        tokens: tokens ?? this.tokens,
        userData: userData);
  }

  @override
  String toString() {
    return "UserState: $tokens, $userData $dataState";
  }
}
