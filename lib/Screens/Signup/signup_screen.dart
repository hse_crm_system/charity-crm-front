import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Signup/components/body.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/tools/api/user_api.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';
import 'package:flutter_hooks/flutter_hooks.dart' show HookWidget;

class SignUpScreen extends HookWidget {
  void onSubmit(RegUserData formData) async {
    try {
      final resp = await UserApi.regUser(formData);
      if (resp['status'] == 'ok') {
        RedirectNavigator.pushAndClean('/login');
        Timer(
            Duration(milliseconds: 600),
            () => {
                  showCustomDialog("Аккаунт создан",
                      'Теперь вы можете войти в созданный аккаунт')
                });
        return;
      }
      if (resp['status'] == 'err') {
        showCustomDialog("Ошибка создания аккаунта", resp['message']);
        return;
      }
    } catch (e) {
      showCustomDialog("Ошибка", 'Что-то пошло не так');
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Body(onSubmit),
      ),
    );
  }
}
