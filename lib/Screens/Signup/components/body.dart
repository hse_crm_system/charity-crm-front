import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Signup/components/background.dart';
import 'package:flutter_crm_app/components/already_have_an_account_acheck.dart';
import 'package:flutter_crm_app/components/rounded_button.dart';
import 'package:flutter_crm_app/components/rounded_input_field.dart';
import 'package:flutter_crm_app/components/rounded_password_field.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/tools/cross_platform_svg.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';
import 'package:flutter_crm_app/tools/validation.dart';

class Body extends StatefulWidget {
  final Function onSubmit;
  const Body(this.onSubmit) : assert(onSubmit != null);
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final _emailController = TextEditingController();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _password2Controller = TextEditingController();
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  bool _isOrganization = false;

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _onSubmit = () {
      if (_emailController.value.text.length == 0 ||
          _passwordController.value.text.length == 0 ||
          _lastNameController.value.text.length == 0 ||
          _usernameController.value.text.length == 0 ||
          _firstNameController.value.text.length == 0) {
        showCustomDialog('Ошибка', 'Вы заполнили не все поля');
        return;
      }
      if (_usernameController.value.text.length < 4) {
        showCustomDialog(
            'Ошибка', 'Имя пользователя не может быть короче четырех символов');
        return;
      }
      if (_password2Controller.value.text != _passwordController.value.text) {
        showCustomDialog('Ошибка', 'Пароли не совпадают');
        return;
      }
      if (!validatePassword(_passwordController.value.text)) {
        showCustomDialog('Слишком простой пароль',
            'Пароль должен содержать строчные и заглавные латинские буквы и специальные спимволы (,;@!#,...)');
        return;
      }
      widget.onSubmit(new RegUserData(
          username: _usernameController.value.text,
          email: _emailController.value.text,
          password: _passwordController.value.text,
          lastName: _lastNameController.value.text,
          firstName: _firstNameController.value.text,
          isOrganisation: _isOrganization));
    };
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 450),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 30),
                Text(
                  "Регистрация",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                      color: kTextColor),
                ),
                SizedBox(height: size.height * 0.03),
                CrossPlatformImage.asset(
                  "assets/images/reg.png",
                  height: size.height * 0.35,
                ),
                RoundedInputField(
                    hintText: "Email",
                    controller: _emailController,
                    onSubmit: (String _) => _onSubmit(),
                    icon: Icons.email_rounded),
                SizedBox(height: 10),
                RoundedInputField(
                    hintText: "Username",
                    controller: _usernameController,
                    onSubmit: (String _) => _onSubmit(),
                    icon: Icons.person),
                SizedBox(height: 10),
                Container(
                  decoration: BoxDecoration(color: kBgDarkColor),
                  width: double.infinity,
                  height: 2,
                ),
                SizedBox(height: 10),
                RoundedInputField(
                  hintText: "Имя",
                  controller: _firstNameController,
                  onSubmit: (String _) => _onSubmit(),
                ),
                SizedBox(height: 10),
                RoundedInputField(
                  hintText: "Фамилия",
                  controller: _lastNameController,
                  onSubmit: (String _) => _onSubmit(),
                ),
                SizedBox(height: 10),
                RoundedPasswordField(
                  controller: _passwordController,
                  onSubmit: (String _) => _onSubmit(),
                ),
                RoundedPasswordField(
                  controller: _password2Controller,
                  onSubmit: (String _) => _onSubmit(),
                  label: 'Повторите пароль',
                ),
                Container(
                  decoration: BoxDecoration(color: kBgDarkColor),
                  width: double.infinity,
                  height: 2,
                ),
                CheckboxListTile(
                  title: Text(
                    "Являюсь физическим лицом",
                    style: TextStyle(color: kTextColor),
                  ),
                  value: !_isOrganization,
                  onChanged: (newValue) {
                    setState(() {
                      _isOrganization = !newValue;
                    });
                  },
                ),
                CheckboxListTile(
                  title: Text(
                    "Являюсь юридическим лицом",
                    style: TextStyle(color: kTextColor),
                  ),
                  value: _isOrganization,
                  onChanged: (newValue) {
                    setState(() {
                      _isOrganization = newValue;
                    });
                  },
                ),
                Container(
                  decoration: BoxDecoration(color: kBgDarkColor),
                  width: double.infinity,
                  height: 2,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15, top: 20),
                  child: Text(
                    "Нажимая на кнопку зарегистрироваться, Вы соглашаетесь с условиями сервиса",
                    style: TextStyle(color: kTextColor, fontSize: 14),
                  ),
                ),
                RoundedButton(text: "Зарегистрироваться", press: _onSubmit),
                SizedBox(height: size.height * 0.03),
                AlreadyHaveAnAccountCheck(
                  login: false,
                  press: () {
                    RedirectNavigator.pushAndClean('/login');
                  },
                ),
                SizedBox(height: 30),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
