import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Loading/loading_screen.dart';
import 'package:flutter_crm_app/Screens/Welcome/components/background.dart';
import 'package:flutter_crm_app/components/rounded_button.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/redux/data_state.dart';
import 'package:flutter_crm_app/tools/cross_platform_svg.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_crm_app/tools/smart_case.dart';

class Body extends StatelessWidget {
  final LoadingState loadingState;
  const Body(this.loadingState);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if (loadingState == LoadingState.loading) {
      return LoadingScreen();
    }
    return Background(
      child: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 450),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: size.height * 0.05),
              Text(
                "Добро пожаловать",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                    color: kTextColor),
              ),
              SizedBox(height: size.height * 0.02),
              Text(
                "GoodFund",
                textAlign: TextAlign.center,
                style: TextStyle(color: kTextColor),
              ),
              SizedBox(height: size.height * 0.03),
              CrossPlatformImage.asset(
                "assets/images/homepage.png",
                height: size.height * 0.45,
              ),
              SizedBox(height: size.height * 0.05),
              Column(
                children: smartCase(loadingState, {
                  LoadingState.loaded: [
                    RoundedButton(
                      text: "Начать",
                      press: () {
                        RedirectNavigator.pushAndClean('/marketplace');
                      },
                    ),
                  ]
                }, [
                  RoundedButton(
                    text: "Регистрация",
                    press: () {
                      RedirectNavigator.pushAndClean('/registration');
                    },
                  ),
                  RoundedButton(
                    text: "Вход",
                    color: kSpaceColor,
                    textColor: kTextColor,
                    press: () {
                      RedirectNavigator.pushAndClean('/login');
                    },
                  ),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
