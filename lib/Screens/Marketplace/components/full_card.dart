import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_crm_app/Screens/Error/error_screen.dart';
import 'package:flutter_crm_app/components/application_reports.dart';
import 'package:flutter_crm_app/components/file_carousel.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/main.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/tools/api/application_api.dart';
import 'package:flutter_crm_app/tools/download_file.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';
import 'package:intl/intl.dart';
import 'package:responsive_grid/responsive_grid.dart';

class FullCard extends StatefulWidget {
  final String id;
  const FullCard(this.id);

  @override
  _FullCardState createState() => _FullCardState();
}

class _FullCardState extends State<FullCard> {
  FullCardData _fullData;
  List<MyDonation> _myDonations = [];
  bool _isLoading = true;
  bool _isError = false;

  @override
  void initState() {
    super.initState();
    _loadApplication();
  }

  void _loadApplication() async {
    try {
      setState(() {
        _isLoading = true;
      });
      final donations =
          await ApplicationApi.getApplicationTransactions(widget.id);
      if (donations != null) {
        setState(() {
          _myDonations = donations;
        });
      } else {
        showCustomDialog(
            'Ошибка', 'Ошибка загрузки Ваших предыдущих пожертований');
      }
      final resp = await ApplicationApi.getApplicationById(widget.id);
      if (resp != null) {
        setState(() {
          _fullData = resp;
        });
      } else {
        setState(() {
          _isError = true;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _isError = true;
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final local = Localizations.localeOf(context).languageCode;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            },
            color: kTextColor,
          ),
          title: Text(
            "Детали заявки",
            style: TextStyle(color: kTextColor),
          ),
          elevation: 0,
          backgroundColor: kBgLightColor,
        ),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(color: kBgLightColor),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(top: 40.0, left: 10, right: 10),
              child: Column(
                children: [
                  if (_isLoading)
                    Center(
                      child: CircularProgressIndicator(),
                    )
                  else if (_isError)
                    ErrorScreen()
                  else if (_fullData != null)
                    Center(
                      child: ResponsiveGridRow(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ResponsiveGridCol(
                            lg: 4,
                            child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 15),
                                              child: Container(
                                                child: Text(
                                                  _fullData.title,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .button
                                                      .copyWith(
                                                          color:
                                                              kTitleTextColor,
                                                          fontSize: 24),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          CircularProgressIndicator(
                                            value: (_fullData.collectedSum /
                                                    (_fullData.targetSum == 0
                                                        ? 1
                                                        : _fullData
                                                            .targetSum)) ??
                                                null,
                                            backgroundColor: kBgDarkColor,
                                          )
                                        ],
                                      ),
                                      Table(
                                        children: [
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 12),
                                              child: Row(
                                                children: [
                                                  Text("Тип заявки"),
                                                  Spacer(),
                                                  Text(_fullData.categoryName),
                                                ],
                                              ),
                                            ),
                                          ]),
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 6),
                                              child: MouseRegion(
                                                cursor:
                                                    SystemMouseCursors.click,
                                                child: GestureDetector(
                                                  onTap: () {
                                                    RedirectNavigator.push(
                                                        '/fund', {
                                                      "id": _fullData.fundId
                                                    });
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      border: Border(
                                                        bottom: BorderSide(
                                                            color: Color(
                                                                0xFFDFE2EF)),
                                                      ),
                                                    ),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              bottom: 10),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            "Фонд",
                                                          ),
                                                          Spacer(),
                                                          Text(
                                                            _fullData.fund,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]),
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 12),
                                              child: Row(
                                                children: [
                                                  Text("Дата окончания сбора"),
                                                  Spacer(),
                                                  Text(DateFormat(
                                                          "dd.MM.yy", local)
                                                      .format(_fullData.endDate
                                                          .toLocal())),
                                                ],
                                              ),
                                            ),
                                          ]),
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 12),
                                              child: Row(
                                                children: [
                                                  Text("Текущая сумма сбора"),
                                                  Spacer(),
                                                  Text(_fullData.collectedSum
                                                      .toString()),
                                                ],
                                              ),
                                            ),
                                          ]),
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 12),
                                              child: Row(
                                                children: [
                                                  Text("Требуемая сумма сбора"),
                                                  Spacer(),
                                                  Text(_fullData.targetSum
                                                      .toString()),
                                                ],
                                              ),
                                            ),
                                          ]),
                                        ],
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                          ResponsiveGridCol(
                            lg: 8,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 18.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        if (_fullData.description.length != 0)
                                          Text(
                                            "ОПИСАНИЕ",
                                            style: Theme.of(context)
                                                .textTheme
                                                .button
                                                .copyWith(
                                                    color: kTextColor,
                                                    fontSize: 14),
                                          ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          _fullData.description,
                                          style: Theme.of(context)
                                              .textTheme
                                              .button
                                              .copyWith(
                                                  color: kTextColor,
                                                  fontSize: 14),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 18.0),
                                      child: FilesCarousel(
                                          files: _fullData.files)),
                                  ApplicationReports(widget.id),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 18.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        if (_myDonations.length != 0)
                                          Text(
                                            "ПРЕДЫДУЩИЕ ПОЖЕРТВОВАНИЯ",
                                            style: Theme.of(context)
                                                .textTheme
                                                .button
                                                .copyWith(
                                                    color: kTextColor,
                                                    fontSize: 14),
                                          ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        ..._myDonations.map(
                                          (e) => MouseRegion(
                                            cursor: SystemMouseCursors.click,
                                            child: GestureDetector(
                                              onTap: () {
                                                downloadFileWeb(
                                                    'https://conflog.infostrategic.com/crm/blockchain?applicationId=${_fullData.id}&structure=Transactions&isMine=1&access=${store.state.userState.tokens.access}');
                                              },
                                              child: Container(
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 12),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      DateFormat(
                                                              "dd.MM.yy", local)
                                                          .format(
                                                              e.date.toLocal()),
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .button
                                                          .copyWith(
                                                              color: kTextColor,
                                                              fontSize: 14),
                                                    ),
                                                    Spacer(),
                                                    Text(
                                                      e.value,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .button
                                                          .copyWith(
                                                              color: kTextColor,
                                                              fontSize: 14),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  TextButton(
                                      onPressed: () {
                                        showPriceDialog(
                                            "Пожертвование",
                                            "Введите сумму, которую Вы хотите пожертвовать",
                                            "Сумма", (String val) async {
                                          final v = double.tryParse(val);
                                          if (v == null) {
                                            throw 'Сумма должна быть числом';
                                          }
                                          if (v < 0) {
                                            throw 'Сумма не может быть меньше 0';
                                          }
                                          try {
                                            final isSuccess =
                                                await ApplicationApi
                                                    .createTransaction(
                                                        _fullData.id, v);
                                            if (isSuccess) {
                                              showCustomDialog('Спасибо',
                                                  "Ваше пожертвование отправлено, спасибо. Вы можете посмотреть информацию по транзакции, нажав на нее");
                                              setState(() {
                                                _myDonations = [
                                                  ..._myDonations,
                                                  new MyDonation(DateTime.now(),
                                                      v.toString())
                                                ];
                                                _fullData.collectedSum +=
                                                    v.ceil();
                                              });
                                            } else {
                                              showCustomDialog('Ошибка',
                                                  "Произошла ошибка отправки пожертвования");
                                            }
                                          } catch (e) {
                                            print(e);
                                            showCustomDialog('Ошибка',
                                                "Произошла неизвестная ошибка");
                                          }
                                        }, "Пожертвовать");
                                      },
                                      child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 18.0),
                                            child: Center(
                                                child: Text("Пожертвовать")),
                                          ))),
                                  SizedBox(
                                    height: 30,
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  else
                    Center(
                      child: Text('Nothing :('),
                    )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
