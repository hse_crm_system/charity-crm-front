import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_crm_app/redux/app_state.dart';
import 'package:flutter_crm_app/redux/data_state.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_redux_hooks/flutter_redux_hooks.dart' as hooks;
import 'package:flutter_hooks/flutter_hooks.dart' show HookWidget, useEffect;

class LoadingScreen extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final loadingState = hooks.useSelector<AppState, LoadingState>(
        (state) => state.userState.dataState?.loadingState);

    useEffect(() {
      if (loadingState == LoadingState.loaded) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          RedirectNavigator.pushAndClean(ModalRoute.of(context).settings.name);
        });
      }
      if (loadingState == LoadingState.error) {
        SchedulerBinding.instance.addPostFrameCallback((_) {
          RedirectNavigator.pushAndClean('/');
        });
      }
      return;
    }, [loadingState]);
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
