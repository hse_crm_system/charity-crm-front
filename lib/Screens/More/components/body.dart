import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Error/error_screen.dart';
import 'package:flutter_crm_app/Screens/Marketplace/components/card_preview.dart';
import 'package:flutter_crm_app/components/rounded_button.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/responsive.dart';
import 'package:flutter_crm_app/tools/api/application_api.dart';
import 'package:flutter_crm_app/tools/api/user_api.dart';
import 'package:flutter_crm_app/tools/cross_platform_svg.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:responsive_grid/responsive_grid.dart';

class Body extends StatefulWidget {
  final UserData userData;
  final GlobalKey<ScaffoldState> scaffoldKey;

  const Body(this.userData, this.scaffoldKey);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  static const _pageSize = 9;
  List<Statistic> statistic;
  bool _isLoading = true;
  bool _isError = false;

  final PagingController<int, List<CardData>> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    _initStatistic();
    super.initState();
  }

  void _initStatistic() async {
    try {
      setState(() {
        _isLoading = true;
      });

      final resp = await UserApi.getUserStatistic();

      if (resp != null) {
        setState(() {
          statistic = resp;
        });
      } else {
        setState(() {
          _isError = true;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _isError = true;
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems =
          await ApplicationApi.getMyTransactedApplications(pageKey, _pageSize);
      // pageKey * _pageSize, _pageSize);
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage([newItems]);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage([newItems], nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    if (widget.userData == null) {
      return ErrorScreen();
    }
    return SafeArea(
        child: DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: AppBar(
            leading: !Responsive.isDesktop(context)
                ? IconButton(
                    icon: Icon(Icons.menu),
                    onPressed: () {
                      widget.scaffoldKey.currentState.openDrawer();
                    },
                    color: kTextColor,
                  )
                : null,
            bottom: TabBar(
              // isScrollable: true,
              // physics: NeverScrollableScrollPhysics(),
              indicatorColor: kBadgeColor,
              tabs: [
                Container(
                  // width: 80,
                  child: Tab(
                      child: Text(
                        'Статистика',
                        style: TextStyle(color: kTextColor),
                      ),
                      icon: Icon(
                        Icons.data_usage_sharp,
                        color: kTextColor,
                      )),
                ),
                Container(
                    // width: 80,
                    child: Tab(
                  icon: Icon(Icons.list, color: kTextColor),
                  child: Text(
                    'История пожертвований',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: kTextColor),
                  ),
                )),
              ],
            ),
            title: Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                "Информация",
                style: TextStyle(color: kTextColor),
              ),
            ),
            elevation: 0,
            backgroundColor: kBgLightColor,
          ),
          body: TabBarView(
            children: [
              Container(
                child: SingleChildScrollView(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 40),
                      child: ConstrainedBox(
                        constraints: BoxConstraints(maxWidth: 450),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "@${widget.userData.username}",
                                  style: Theme.of(context)
                                      .textTheme
                                      .button
                                      .copyWith(
                                          color: kTitleTextColor, fontSize: 24),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                if (widget.userData.firstName != null &&
                                    widget.userData.lastName != null)
                                  Text(
                                    "${widget.userData.firstName} ${widget.userData.lastName}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .button
                                        .copyWith(
                                            color: kTitleTextColor,
                                            fontSize: 16),
                                  ),
                                SizedBox(
                                  height: 20,
                                ),
                                if (_isError)
                                  Center(
                                    child: Text(
                                      "",
                                      style: TextStyle(color: kTextColor),
                                    ),
                                  )
                                else if (_isLoading)
                                  Center(child: CircularProgressIndicator())
                                else if (statistic != null)
                                  Table(
                                    children: [
                                      ...statistic.map(
                                        (e) => TableRow(children: [
                                          Container(
                                            margin: EdgeInsets.symmetric(
                                                vertical: 12),
                                            child: Row(
                                              children: [
                                                Text(e.title),
                                                Spacer(),
                                                Text(e.value),
                                              ],
                                            ),
                                          ),
                                        ]),
                                      )
                                    ],
                                  ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                child: Scrollbar(
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: Responsive.isMobile(context) ? 15 : 40),
                    child: PagedListView<int, List<CardData>>(
                      shrinkWrap: true,
                      pagingController: _pagingController,
                      builderDelegate:
                          PagedChildBuilderDelegate<List<CardData>>(
                              firstPageErrorIndicatorBuilder: (context) =>
                                  Center(
                                    child: Text("Произошла ошибка загрузки"),
                                  ),
                              newPageErrorIndicatorBuilder: (context) => Center(
                                    child: Text("Произошла ошибка :("),
                                  ),
                              noMoreItemsIndicatorBuilder: (context) => Center(
                                      child: ConstrainedBox(
                                    constraints: BoxConstraints(maxWidth: 450),
                                    child: Column(
                                      children: [
                                        SizedBox(height: 30),
                                        CrossPlatformImage.asset(
                                          "assets/images/login.png",
                                          width: min(450, size.width - 20),
                                        ),
                                        RoundedButton(
                                            text:
                                                "Отправиться на поиск нуждающихся",
                                            press: () {
                                              RedirectNavigator.pushAndClean(
                                                  "/marketplace");
                                            })
                                      ],
                                    ),
                                  )),
                              itemBuilder: (context, items, index) =>
                                  ResponsiveGridRow(
                                      children: items
                                          .map((e) => ResponsiveGridCol(
                                                lg: 4,
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 20,
                                                      horizontal: 10),
                                                  child: new CardPreview(e),
                                                ),
                                              ))
                                          .toList())),
                    ),
                  ),
                ),
              )
            ],
          )),
    ));
  }
}
