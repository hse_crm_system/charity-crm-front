import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_crm_app/Screens/Error/error_screen.dart';
import 'package:flutter_crm_app/components/category_dropdown.dart';
import 'package:flutter_crm_app/components/file_carousel.dart';
import 'package:flutter_crm_app/components/file_pick.dart';
import 'package:flutter_crm_app/components/rounded_button.dart';
import 'package:flutter_crm_app/components/rounded_input_field.dart';
import 'package:flutter_crm_app/components/send_beneficiary_request.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/responsive.dart';
import 'package:flutter_crm_app/tools/api/application_api.dart';
import 'package:flutter_crm_app/tools/api/fund_api.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';

class Body extends StatefulWidget {
  final UserData userData;
  const Body(this.userData);
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  Timer _debounce;
  final _titleController = new TextEditingController();
  List<MediaFile> _files = [];
  DateTime _endDate;
  final _descriptionController = new TextEditingController();
  final _fundController = new TextEditingController();
  Category _selectedCategory;
  final _sumController = new TextEditingController();
  List<FundPreview> _foundFunds = [];
  List<FundPreview> _selectedFunds = [];
  bool _isLoading = false;

  bool isLoading = true;
  bool isError = false;

  @override
  void initState() {
    super.initState();
    _getFunds();
    setState(() {
      isLoading = false;
    });
  }

  void _createHandler() async {
    if (_selectedCategory == null || _selectedCategory?.id == null) {
      showCustomDialog('Ошибка', "Вы забыли выбрать категорию");
      return;
    }
    if (_endDate == null) {
      showCustomDialog('Ошибка', "Вы забыли выбрать дату окончания сбора");
      return;
    }
    if (_titleController.value.text.isEmpty ||
        _descriptionController.value.text.isEmpty ||
        _sumController.value.text.isEmpty) {
      showCustomDialog('Ошибка', "Вы заполнили не все поля");
      return;
    }
    if (_selectedFunds.isEmpty) {
      showCustomDialog('Ошибка',
          "Вы забыли выбрать фонды, которые будут рассматривать Вашу заявку");
      return;
    }
    if (int.tryParse(_sumController.value.text) == null ||
        int.tryParse(_sumController.value.text) <= 0) {
      showCustomDialog('Ошибка',
          "Сумма заявки может быть только целым положительным числом");
      return;
    }
    try {
      setState(() {
        _isLoading = true;
      });
      final isSuccess = await ApplicationApi.createApplication(
          new ApplicationCreation(
              _titleController.value.text,
              "0",
              "",
              _endDate,
              0,
              int.parse(_sumController.value.text),
              _selectedCategory.id,
              _descriptionController.value.text,
              _files,
              _selectedFunds.map((e) => e.id).toList()));
      if (!isSuccess) {
        showCustomDialog('Ошибка', "Ошибка создания заявки");
        return;
      }
      Navigator.pop(context);
      Timer(Duration(microseconds: 600), () {
        showCustomDialog('Поздравлем', "Заявка отправлена на модерацию");
      });
    } catch (e) {
      print(e);
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _getFunds() async {
    try {
      if (_selectedCategory?.id != null) {
        final funds = await FundApi.getFunds(
            _fundController.value.text, _selectedCategory?.id);
        if (funds != null) {
          setState(() {
            _foundFunds = funds.map((e) => FundPreview(e.name, e.id)).toList();
          });
        }
      } else {
        setState(() {
          _foundFunds = [];
          _selectedFunds = [];
        });
      }
    } catch (e) {
      print(e);
    }
  }

  void _selectFund(FundPreview x) {
    if (_selectedCategory?.id == null) {
      showCustomDialog('Ошибка',
          'Перед выбором фондов, рассматривающих Вашу заявку, необходимо выбрать категорию');
      return;
    }
    setState(() {
      if (_selectedFunds.map((e) => e.id).contains(x.id)) {
        _selectedFunds =
            _selectedFunds.where((element) => element.id != x.id).toList();
      } else {
        _selectedFunds = [..._selectedFunds, x];
      }
    });
  }

  _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 500), () {
      _getFunds();
    });
  }

  @override
  void dispose() {
    _debounce?.cancel();
    _sumController.dispose();
    _fundController.dispose();
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final local = Localizations.localeOf(context).languageCode;

    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    if (isError || widget.userData == null) {
      return ErrorScreen();
    }

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            },
            color: kTextColor,
          ),
          title: Text(
            "Создание заявки",
            style: TextStyle(color: kTextColor),
          ),
          elevation: 0,
          backgroundColor: kBgLightColor,
        ),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          child: SingleChildScrollView(
            child: Center(
                child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Responsive.isMobile(context) ? 15 : 30,
                      vertical: 10),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(maxWidth: 450),
                    child: (!widget.userData.role.contains('crm_beneficiary'))
                        ? SendBeneficiaryRequest()
                        : Column(
                            children: [
                              SizedBox(
                                height: 40,
                              ),
                              CategoryDropdown((c) {
                                setState(() {
                                  _selectedCategory = c;
                                  _selectedFunds = [];
                                });
                                _getFunds();
                              }, true),
                              SizedBox(
                                height: 10,
                              ),
                              RoundedInputField(
                                controller: _titleController,
                                hintText: "Название сбора",
                                icon: Icons.flag_rounded,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  if (_endDate != null)
                                    Text(DateFormat("dd.MM.yyyy", local)
                                        .format(_endDate.toLocal())),
                                  Spacer(),
                                  TextButton(
                                      onPressed: () {
                                        DatePicker.showDatePicker(context,
                                            showTitleActions: true,
                                            onConfirm: (date) {
                                          setState(() {
                                            _endDate = date;
                                          });
                                        },
                                            currentTime: DateTime.now(),
                                            locale: LocaleType.ru);
                                      },
                                      child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 14.0, horizontal: 20),
                                            child: Center(
                                                child: Text(
                                              "${(_endDate == null) ? "Выбрать" : "Изменить"} дату окончания сбора",
                                              style:
                                                  TextStyle(color: kGrayColor),
                                            )),
                                          ))),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              RoundedInputField(
                                  controller: _sumController,
                                  icon: Icons.monetization_on_sharp,
                                  hintText: "Сумма сбора"),
                              SizedBox(
                                height: 10,
                              ),
                              RoundedInputField(
                                  controller: _descriptionController,
                                  icon: Icons.description,
                                  isMultiline: true,
                                  hintText: "Описание сбора"),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                width: double.infinity,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          top: 20.0, bottom: 10),
                                      child: Text(
                                        "ВЫБОР ФОНДОВ",
                                        textAlign: TextAlign.start,
                                        style: Theme.of(context)
                                            .textTheme
                                            .button
                                            .copyWith(
                                                color: kTextColor,
                                                fontSize: 14),
                                      ),
                                    ),
                                    Text(
                                      "Выберите фонды, в которые Вы хотите подать заявку",
                                      style: TextStyle(
                                          color: kGrayColor,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              ),
                              RoundedInputField(
                                controller: _fundController,
                                icon: Icons.group,
                                hintText: "Поиск",
                                onChanged: (String val) {
                                  _onSearchChanged();
                                },
                              ),
                              Table(
                                children: [
                                  ..._foundFunds.map(
                                    (e) => TableRow(children: [
                                      MouseRegion(
                                        cursor: SystemMouseCursors.click,
                                        child: GestureDetector(
                                          onTap: () {
                                            _selectFund(e);
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 12),
                                            child: Row(
                                              children: [
                                                Row(
                                                  children: [
                                                    _selectedFunds.any(
                                                            (element) =>
                                                                element.id ==
                                                                e.id)
                                                        ? Icon(
                                                            Icons
                                                                .check_circle_rounded,
                                                            size: 16.0,
                                                            color: kTextColor,
                                                          )
                                                        : Icon(
                                                            Icons
                                                                .blur_circular_sharp,
                                                            size: 16.0,
                                                            color: kTextColor,
                                                          ),
                                                    SizedBox(
                                                      width: 16,
                                                    ),
                                                    Text("${e.name}",
                                                        style: TextStyle(
                                                            color: kTextColor)),
                                                  ],
                                                ),
                                                Spacer(),
                                                Text("id: ${e.id}",
                                                    style: TextStyle(
                                                        color: kTextColor)),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ]),
                                  )
                                ],
                              ),
                              if (_selectedFunds.isNotEmpty)
                                Container(
                                  width: double.infinity,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 20.0, bottom: 10),
                                        child: Text(
                                          "ВЫБРАННЫЕ ФОНДЫ",
                                          textAlign: TextAlign.start,
                                          style: Theme.of(context)
                                              .textTheme
                                              .button
                                              .copyWith(
                                                  color: kTextColor,
                                                  fontSize: 14),
                                        ),
                                      ),
                                      Text(
                                        "Нажмите на фонд, чтобы удалить его",
                                        style: TextStyle(
                                            color: kGrayColor,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ],
                                  ),
                                ),
                              Table(
                                children: [
                                  ..._selectedFunds.map(
                                    (e) => TableRow(children: [
                                      MouseRegion(
                                        cursor: SystemMouseCursors.click,
                                        child: GestureDetector(
                                          onTap: () {
                                            _selectFund(e);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                                vertical: 12),
                                            child: Row(
                                              children: [
                                                Row(
                                                  children: [
                                                    Icon(
                                                      Icons
                                                          .remove_circle_outline,
                                                      size: 16.0,
                                                      color: kBadgeColor,
                                                    ),
                                                    SizedBox(
                                                      width: 16,
                                                    ),
                                                    Text(
                                                      "${e.name}",
                                                      style: TextStyle(
                                                          color: kTextColor),
                                                    ),
                                                  ],
                                                ),
                                                Spacer(),
                                                Text(
                                                  "id: ${e.id}",
                                                  style: TextStyle(
                                                      color: kTextColor),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ]),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              FilePick((f) {
                                setState(() {
                                  _files = [..._files, ...f];
                                });
                              }),
                              SizedBox(
                                height: 10,
                              ),
                              FilesCarousel(
                                files: _files,
                                isRemoveable: true,
                                onRemove: (e) {
                                  setState(() {
                                    _files = _files
                                        .where((element) => element != e)
                                        .toList();
                                  });
                                },
                              ),
                              RoundedButton(
                                text: "Отправить заявку",
                                press: _createHandler,
                                isDisabled: _isLoading,
                              ),
                            ],
                          ),
                  ),
                ),
              ),
            )),
          ),
        ),
      ),
    );
  }
}
