import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Create/components/body.dart';
import 'package:flutter_crm_app/components/side_menu.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/redux/app_state.dart';
import 'package:flutter_crm_app/responsive.dart';
import 'package:flutter_redux_hooks/flutter_redux_hooks.dart' as hooks;
import 'package:flutter_hooks/flutter_hooks.dart' show HookWidget;

class Create extends HookWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;

    final userData = hooks
        .useSelector<AppState, UserData>((state) => state.userState.userData);
    return Scaffold(
      key: _scaffoldKey,
      drawer: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 250),
        child: SideMenu(),
      ),
      body: Responsive(
        mobile: Body(userData),
        tablet: Body(userData),
        desktop: Row(
          children: [
            Expanded(
              flex: _size.width > 1340 ? 2 : 4,
              child: SideMenu(),
            ),
            Expanded(
              flex: _size.width > 1340 ? 11 : 15,
              child: Body(userData),
            ),
          ],
        ),
      ),
    );
  }
}
