import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_crm_app/Screens/Error/error_screen.dart';
import 'package:flutter_crm_app/components/application_reports.dart';
import 'package:flutter_crm_app/components/chat_message.dart';
import 'package:flutter_crm_app/components/file_carousel.dart';
import 'package:flutter_crm_app/components/file_pick.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/responsive.dart';
import 'package:flutter_crm_app/tools/api/application_api.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_crm_app/tools/show_dialog.dart';
import 'package:intl/intl.dart';
import 'package:responsive_grid/responsive_grid.dart';

class FullCard extends StatefulWidget {
  final String id;
  const FullCard(this.id);

  @override
  _FullCardState createState() => _FullCardState();
}

class _FullCardState extends State<FullCard> {
  List<MediaFile> _selectedFiles = [];
  FullCardData _fullData;
  List<Message> _chatData = [];
  bool _isLoading = true;
  bool _isError = false;
  final _chatMessageController = TextEditingController();
  void _onSubmit() async {
    if (_chatMessageController.value.text.isEmpty) {
      return;
    }
    try {
      final newFiles = await ApplicationApi.sendMessage(
          widget.id,
          new Message(DateTime.now(), _chatMessageController.value.text, "Вы",
              _selectedFiles));
      if (newFiles == null) {
        showCustomDialog("Ошибка", "Произошла ошибка отправки сообщения");
        return;
      }
      setState(() {
        _chatData = [
          new Message(DateTime.now(), _chatMessageController.value.text, "Вы",
              newFiles),
          ..._chatData
        ];
        _chatMessageController.clear();
        _selectedFiles = [];
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    _chatMessageController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _loadApplication();
  }

  void _loadApplication() async {
    try {
      setState(() {
        _isLoading = true;
      });
      final chatResp = await ApplicationApi.getApplicationChat(widget.id);
      setState(() {
        _chatData = chatResp;
      });
      final resp = await ApplicationApi.getApplicationById(widget.id);
      if (resp != null) {
        setState(() {
          _fullData = resp;
        });
      } else {
        setState(() {
          _isError = true;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _isError = true;
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final local = Localizations.localeOf(context).languageCode;
    Size size = MediaQuery.of(context).size;

    if (_isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    if (_isError) {
      return ErrorScreen();
    }

    var ChatForm = Padding(
      padding: const EdgeInsets.only(top: 27.0, left: 10, right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    autofocus: false,
                    style: TextStyle(color: kGrayColor),
                    controller: _chatMessageController,
                    onSubmitted: (String _) => _onSubmit(),
                    decoration: InputDecoration(
                      hintText: "Ваше сообщение",
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
          Row(
            children: [
              TextButton(
                  onPressed: _onSubmit,
                  child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 14.0, horizontal: 20),
                        child: Center(
                            child: Text("Отправить",
                                style: TextStyle(color: kBadgeColor))),
                      ))),
              SizedBox(
                width: 20,
              ),
              FilePick((f) {
                setState(() {
                  _selectedFiles = [..._selectedFiles, ...f];
                });
              }),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          FilesCarousel(
            files: _selectedFiles,
            isRemoveable: true,
            onRemove: (f) {
              setState(() {
                _selectedFiles =
                    _selectedFiles.where((element) => element != f).toList();
              });
            },
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
    var ChatMessages = Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            width: double.infinity,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ..._chatData.map((e) => Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: ChatMessage(e),
                      )),
                  SizedBox(
                    height: 30,
                  )
                ]),
          ),
        ),
      ),
    );
    return SafeArea(
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
              color: kTextColor,
            ),
            bottom: TabBar(
              // isScrollable: true,
              // physics: NeverScrollableScrollPhysics(),
              indicatorColor: kBadgeColor,
              tabs: [
                Container(
                  // width: 80,
                  child: Tab(
                      child: Text(
                        'Основная информация',
                        style: TextStyle(color: kTextColor),
                        textAlign: TextAlign.center,
                      ),
                      icon: Icon(
                        Icons.home_rounded,
                        color: kTextColor,
                      )),
                ),
                Container(
                    // width: 80,
                    child: Tab(
                        child: Text(
                          'Чат',
                          style: TextStyle(color: kTextColor),
                        ),
                        icon: Icon(Icons.chat_rounded, color: kTextColor))),
              ],
            ),
            // title: Padding(
            //   padding: const EdgeInsets.only(top: 8.0),
            //   child: Text(
            //     "Детали заявки",
            //     style: TextStyle(color: kTextColor, fontSize: 14),
            //   ),
            // ),
            elevation: 0,
            backgroundColor: kBgLightColor,
          ),
          body: TabBarView(
            children: [
              Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(color: kBgLightColor),
                child: SingleChildScrollView(
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 40.0, left: 10, right: 10),
                    child: Center(
                      child: ResponsiveGridRow(
                        children: [
                          ResponsiveGridCol(
                            lg: 4,
                            child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 15),
                                              child: Container(
                                                child: Text(
                                                  _fullData.title,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .button
                                                      .copyWith(
                                                          color:
                                                              kTitleTextColor,
                                                          fontSize: 24),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(width: 10),
                                          CircularProgressIndicator(
                                            value: (_fullData.collectedSum /
                                                    (_fullData.targetSum == 0
                                                        ? 1
                                                        : _fullData
                                                            .targetSum)) ??
                                                null,
                                            backgroundColor: kBgDarkColor,
                                          )
                                        ],
                                      ),
                                      Table(
                                        children: [
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 12),
                                              child: Row(
                                                children: [
                                                  Text("Категория"),
                                                  Spacer(),
                                                  Text(_fullData.categoryName),
                                                ],
                                              ),
                                            ),
                                          ]),
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 6),
                                              child: MouseRegion(
                                                cursor:
                                                    SystemMouseCursors.click,
                                                child: GestureDetector(
                                                  onTap: () {
                                                    RedirectNavigator.push(
                                                        '/fund', {
                                                      "id": _fullData.fundId
                                                    });
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      border: Border(
                                                        bottom: BorderSide(
                                                            color: Color(
                                                                0xFFDFE2EF)),
                                                      ),
                                                    ),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              bottom: 10),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            "Фонд",
                                                          ),
                                                          Spacer(),
                                                          Text(
                                                            _fullData.fund,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]),
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 12),
                                              child: Row(
                                                children: [
                                                  Text("Дата окончания сбора"),
                                                  Spacer(),
                                                  Text(DateFormat(
                                                          "dd.MM.yy", local)
                                                      .format(_fullData.endDate
                                                          .toLocal())),
                                                ],
                                              ),
                                            ),
                                          ]),
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 12),
                                              child: Row(
                                                children: [
                                                  Text("Текущая сумма сбора"),
                                                  Spacer(),
                                                  Text(_fullData.collectedSum
                                                      .toString()),
                                                ],
                                              ),
                                            ),
                                          ]),
                                          TableRow(children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                  vertical: 12),
                                              child: Row(
                                                children: [
                                                  Text("Требуемая сумма сбора"),
                                                  Spacer(),
                                                  Text(_fullData.targetSum
                                                      .toString()),
                                                ],
                                              ),
                                            ),
                                          ]),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      if (_fullData.status != null)
                                        Container(
                                          width: double.infinity,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              color: kBgDarkColor),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 8.0, horizontal: 14),
                                            child: Text(
                                              getStringStatus(_fullData.status),
                                              textAlign: TextAlign.center,
                                              style:
                                                  TextStyle(color: kTextColor),
                                            ),
                                          ),
                                        ),
                                    ],
                                  ),
                                )),
                          ),
                          ResponsiveGridCol(
                            lg: 8,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 18.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        _fullData.description.length != 0
                                            ? Text(
                                                "ОПИСАНИЕ",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .button
                                                    .copyWith(
                                                        color: kTextColor,
                                                        fontSize: 14),
                                              )
                                            : null,
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          _fullData.description,
                                          style: Theme.of(context)
                                              .textTheme
                                              .button
                                              .copyWith(
                                                  color: kTextColor,
                                                  fontSize: 14),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 18.0),
                                      child: FilesCarousel(
                                          files: _fullData.files)),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  ApplicationReports(widget.id),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                  width: double.infinity,
                  // height: double.infinity,
                  decoration: BoxDecoration(color: kBgLightColor),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Responsive(
                      mobile: SingleChildScrollView(
                        child: Column(
                          children: [ChatForm, ChatMessages],
                        ),
                      ),
                      tablet: Row(
                        children: [
                          Expanded(
                            flex: 6,
                            child: ChatForm,
                          ),
                          Expanded(
                            flex: 8,
                            child: ChatMessages,
                          ),
                        ],
                      ),
                      desktop: Row(
                        children: [
                          Expanded(
                            flex: 4,
                            child: ChatForm,
                          ),
                          Expanded(
                            flex: 8,
                            child: ChatMessages,
                          ),
                        ],
                      ),
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
