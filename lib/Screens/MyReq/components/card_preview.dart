import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:intl/intl.dart';

class CardPreview extends StatelessWidget {
  final CardData data;
  const CardPreview(this.data);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final local = Localizations.localeOf(context).languageCode;
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: () {
          RedirectNavigator.push('/myReq', {'id': data.id});
        },
        child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Container(
                            child: Text(
                              data.title,
                              style: Theme.of(context)
                                  .textTheme
                                  .button
                                  .copyWith(
                                      color: kGrayColor,
                                      fontSize:
                                          data.title.length > 20 ? 20 : 24),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      CircularProgressIndicator(
                        value: (data.targetSum == 0)
                            ? 0
                            : ((data.collectedSum / data.targetSum) ?? 0),
                        backgroundColor: kBgDarkColor,
                      )
                    ],
                  ),
                  Table(
                    children: [
                      TableRow(children: [
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 12),
                          child: Row(
                            children: [
                              Text("Тип заявки"),
                              Spacer(),
                              Text(data.categoryName),
                            ],
                          ),
                        ),
                      ]),
                      TableRow(children: [
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 12),
                          child: Row(
                            children: [
                              Text("Фонд"),
                              Spacer(),
                              Text(data.fund),
                            ],
                          ),
                        ),
                      ]),
                      TableRow(children: [
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 12),
                          child: Row(
                            children: [
                              Text("Дата окончания сбора"),
                              Spacer(),
                              Text(DateFormat("dd.MM.yy", local)
                                  .format(data.endDate.toLocal())),
                            ],
                          ),
                        ),
                      ]),
                      TableRow(children: [
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 12),
                          child: Row(
                            children: [
                              Text("Текущая сумма сбора"),
                              Spacer(),
                              Text(data.collectedSum.toString()),
                            ],
                          ),
                        ),
                      ]),
                      TableRow(children: [
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 12),
                          child: Row(
                            children: [
                              Text("Требуемая сумма сбора"),
                              Spacer(),
                              Text(data.targetSum.toString()),
                            ],
                          ),
                        ),
                      ])
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  if (data.status != null)
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: kBgDarkColor),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 14),
                        child: Text(
                          getStringStatus(data.status),
                          textAlign: TextAlign.center,
                          style: TextStyle(color: kTextColor),
                        ),
                      ),
                    ),
                ],
              ),
            )),
      ),
    );
  }
}
