import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Error/error_screen.dart';
import 'package:flutter_crm_app/Screens/MyReq/components/card_preview.dart';
import 'package:flutter_crm_app/components/category_dropdown.dart';
import 'package:flutter_crm_app/components/rounded_button.dart';
import 'package:flutter_crm_app/components/send_beneficiary_request.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/models/login_user.dart';
import 'package:flutter_crm_app/responsive.dart';
import 'package:flutter_crm_app/tools/api/application_api.dart';
import 'package:flutter_crm_app/tools/cross_platform_svg.dart';
import 'package:flutter_crm_app/tools/download_file.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:responsive_grid/responsive_grid.dart';

class Body extends StatefulWidget {
  final UserData userData;
  final GlobalKey<ScaffoldState> scaffoldKey;
  const Body(this.userData, this.scaffoldKey);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  static const _pageSize = 9;
  // ignore: unused_field
  Category _selectedCategory;
  final PagingController<int, List<CardData>> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await ApplicationApi.getMyApplications(
          pageKey, _pageSize, _selectedCategory?.id);
      // pageKey * _pageSize, _pageSize, _selectedCategory.id);
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage([newItems]);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage([newItems], nextPageKey);
      }
    } catch (error) {
      print(error);
      _pagingController.error = error;
    }
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    if (widget.userData == null) {
      return ErrorScreen();
    }

    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        leading: !Responsive.isDesktop(context)
            ? IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {
                  widget.scaffoldKey.currentState.openDrawer();
                },
                color: kTextColor,
              )
            : null,
        title: Row(
          children: [
            Text(
              "Мои заявки",
              style: TextStyle(color: kTextColor),
            ),
            Spacer(),
            if (widget.userData.role.contains('crm_beneficiary'))
              CategoryDropdown((c) {
                setState(() {
                  _selectedCategory = c;
                });
                _pagingController.refresh();
              }),
          ],
        ),
        elevation: 0,
        backgroundColor: kBgLightColor,
      ),
      body: !widget.userData.role.contains('crm_beneficiary')
          ? SingleChildScrollView(
              child: Center(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: Responsive.isMobile(context) ? 15 : 30,
                          vertical: 10),
                      child: ConstrainedBox(
                          constraints: BoxConstraints(maxWidth: 450),
                          child: SendBeneficiaryRequest()),
                    ),
                  ),
                ),
              ),
            )
          : Scrollbar(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: Responsive.isMobile(context) ? 15 : 40),
                child: PagedListView<int, List<CardData>>(
                  shrinkWrap: true,
                  pagingController: _pagingController,
                  builderDelegate: PagedChildBuilderDelegate<List<CardData>>(
                      firstPageErrorIndicatorBuilder: (context) => Center(
                            child: ErrorScreen(),
                          ),
                      newPageErrorIndicatorBuilder: (context) => Center(
                            child: Text("Произошла ошибка :("),
                          ),
                      noMoreItemsIndicatorBuilder: (context) => Center(
                              child: ConstrainedBox(
                            constraints: BoxConstraints(maxWidth: 450),
                            child: Column(
                              children: [
                                SizedBox(height: 30),
                                CrossPlatformImage.asset(
                                  "assets/images/homepage.png",
                                  width: min(450, size.width - 20),
                                ),
                                RoundedButton(
                                    text: "Отправиться на поиск нуждающихся",
                                    press: () {
                                      RedirectNavigator.pushAndClean(
                                          "/marketplace");
                                    })
                              ],
                            ),
                          )),
                      itemBuilder: (context, items, index) => ResponsiveGridRow(
                          children: items
                              .map((e) => ResponsiveGridCol(
                                    lg: 4,
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 20, horizontal: 10),
                                      child: new CardPreview(e),
                                    ),
                                  ))
                              .toList())),
                ),
              ),
            ),
    ));
  }
}
