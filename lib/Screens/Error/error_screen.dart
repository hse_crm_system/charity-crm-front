import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_crm_app/components/rounded_button.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/tools/cross_platform_svg.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';

class ErrorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SafeArea(
      child: SingleChildScrollView(
        child: Center(
          child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 450),
            child: Container(
              child: Column(
                children: [
                  SizedBox(height: 30),
                  CrossPlatformImage.asset(
                    "assets/images/error.png",
                    width: min(450, size.width - 20),
                  ),
                  Text(
                    "Произошла ошибка",
                    style: TextStyle(fontSize: 24, color: kTextColor),
                  ),
                  SizedBox(height: 30),
                  RoundedButton(
                      text: "На главную страницу",
                      press: () {
                        RedirectNavigator.pushAndClean("/");
                      }),
                  SizedBox(height: 30),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
