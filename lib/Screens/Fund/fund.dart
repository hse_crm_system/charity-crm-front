import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_crm_app/Screens/Fund/components/body.dart';
import 'package:flutter_crm_app/components/side_menu.dart';
import 'package:flutter_crm_app/responsive.dart';
import 'package:flutter_crm_app/tools/redirect_navigator.dart';
import 'package:flutter_hooks/flutter_hooks.dart' show HookWidget;

class Fund extends HookWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    final args =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    final id = args != null ? args['id'] : null;
    if (id == null) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        RedirectNavigator.pushAndClean('/');
      });
    }

    return Scaffold(
      key: _scaffoldKey,
      drawer: ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 250),
        child: SideMenu(),
      ),
      body: Responsive(
        mobile: Body(id),
        tablet: Body(id),
        desktop: Row(
          children: [
            Expanded(
              flex: _size.width > 1340 ? 2 : 4,
              child: SideMenu(),
            ),
            Expanded(
              flex: _size.width > 1340 ? 11 : 15,
              child: Body(id),
            ),
          ],
        ),
      ),
    );
  }
}
