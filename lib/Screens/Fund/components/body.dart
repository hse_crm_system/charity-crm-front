import 'package:flutter/material.dart';
import 'package:flutter_crm_app/Screens/Error/error_screen.dart';
import 'package:flutter_crm_app/constants.dart';
import 'package:flutter_crm_app/models/cards.dart';
import 'package:flutter_crm_app/tools/api/fund_api.dart';
import 'package:responsive_grid/responsive_grid.dart';

class Body extends StatefulWidget {
  final String id;
  const Body(this.id);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  FundModel _fullData;

  bool _isLoading = true;
  bool _isError = false;

  @override
  void initState() {
    super.initState();
    _initLoading();
  }

  void _initLoading() async {
    if (widget.id == "-1") {
      return;
    }
    try {
      setState(() {
        _isLoading = true;
      });

      final resp = await FundApi.getFundById(widget.id);
      if (resp != null) {
        setState(() {
          _fullData = resp;
        });
      } else {
        setState(() {
          _isError = true;
        });
      }
    } catch (e) {
      print(e);
      setState(() {
        _isError = true;
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.id == "-1" || widget.id == null) {
      return SafeArea(
          child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            },
            color: kTextColor,
          ),
          title: Text(
            "Детали фонда",
            style: TextStyle(color: kTextColor),
          ),
          elevation: 0,
          backgroundColor: kBgLightColor,
        ),
        body: Center(
          child: ConstrainedBox(
              constraints: BoxConstraints(maxWidth: 450),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Нет фонда...",
                      textAlign: TextAlign.center,
                      style: Theme.of(context)
                          .textTheme
                          .button
                          .copyWith(color: kTitleTextColor, fontSize: 24),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Видимо, никакой фонд еще не взял в работу эту заявку",
                      textAlign: TextAlign.center,
                      style: Theme.of(context)
                          .textTheme
                          .button
                          .copyWith(color: kTextColor, fontSize: 18),
                    )
                  ],
                ),
              )),
        ),
      ));
    }
    if (_isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    if (_isError) {
      return ErrorScreen();
    }

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () {
              Navigator.pop(context);
            },
            color: kTextColor,
          ),
          title: Text(
            "Детали фонда",
            style: TextStyle(color: kTextColor),
          ),
          elevation: 0,
          backgroundColor: kBgLightColor,
        ),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(color: kBgLightColor),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(top: 40.0, left: 10, right: 10),
              child: Center(
                child: ResponsiveGridRow(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ResponsiveGridCol(
                      lg: 4,
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 15),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border(
                                              bottom: BorderSide(
                                                  color: Color(0xFFDFE2EF)),
                                            ),
                                          ),
                                          child: Text(
                                            "Фонд: ${_fullData.name}",
                                            style: Theme.of(context)
                                                .textTheme
                                                .button
                                                .copyWith(
                                                    color: kTitleTextColor,
                                                    fontSize: 24),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Text(_fullData.types.join(', ')),
                                SizedBox(height: 20),
                                Table(
                                  children: [
                                    ..._fullData.statistic.map(
                                      (e) => TableRow(children: [
                                        Container(
                                          margin: EdgeInsets.symmetric(
                                              vertical: 12),
                                          child: Row(
                                            children: [
                                              Text(e.title ?? "Нет значения"),
                                              Spacer(),
                                              Text(e.value ?? "Нет значения"),
                                            ],
                                          ),
                                        ),
                                      ]),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          )),
                    ),
                    ResponsiveGridCol(
                      lg: 8,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 18.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  _fullData.description.length != 0
                                      ? Text(
                                          "ОПИСАНИЕ",
                                          style: Theme.of(context)
                                              .textTheme
                                              .button
                                              .copyWith(
                                                  color: kTextColor,
                                                  fontSize: 14),
                                        )
                                      : SizedBox(
                                          height: 10,
                                        ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    _fullData.description,
                                    style: Theme.of(context)
                                        .textTheme
                                        .button
                                        .copyWith(
                                            color: kTextColor, fontSize: 14),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
